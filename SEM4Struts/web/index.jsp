<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html lang="vi">

    <head>
        <meta charset="UTF-8">
        <link rel="shortcut icon" type="image/png" href="./style/favicon.ico"/>
        <title>AptechAir - Ngôi sao trên trời</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="./style/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="./style/style.css">
        <script src="/style/js/jquery-1.12.4.min.js"></script>
        <!--   <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script> -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
        <script src="./style/js/bootstrap.js"></script>
        <!-- //// Select -->
        <script src="./style/select/jquery.minimalect.min.js"></script>
        <link rel="stylesheet" href="./style/select/minimalect.css">
        <link rel="stylesheet" href="./style/datepicker-ui.css">
        <link rel="stylesheet" type="text/css" href="./style/select/easydropdown.css" />
        <script src="./style/select/jquery.easydropdown.js"></script>
        <script src="/style/js/jquery-ui-1.12.1-min.js"></script>
        <script type="text/javascript">
            $(function () {
                $("#d1").minimalect();
                $("#d2").minimalect();
                $(document).tooltip({
                    position: {
                        my: "center bottom-10",
                        at: "center top",
                    }
                });
            });
        </script>
        <script>
            $(function () {
                var dateFormat = "dd-mm-yy",
                        from = $("#flightDate")
                        .datepicker({
                            defaultDate: "+1w",
                            numberOfMonths: 2,
                            // showButtonPanel: true,
                            currentText: "Tháng này",
                            closeText: 'Đóng',
                            minDate: 0,
                            dateFormat: 'dd-mm-yy',
                            // defaultDate: +7,
                            // minDate: 0,
                            dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
                            monthNames: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
                        })
                        .datepicker("setDate", new Date())
                        .on("change", function () {
                            to.datepicker("option", "minDate", getDate(this));
                        }),
                        to = $("#returnDate").datepicker({
                    defaultDate: "+1w",
                    numberOfMonths: 2,
                    minDate: 0,
                    currentText: "Tháng này",
                    closeText: 'Đóng',
                    dateFormat: 'dd-mm-yy',
                    // defaultDate: +7,
                    // minDate: 0,
                    dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
                    monthNames: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
                })
                        .datepicker("setDate", new Date());

                function getDate(element) {
                    var date;
                    try {
                        date = $.datepicker.parseDate(dateFormat, element.value);
                    } catch (error) {
                        date = null;
                    }

                    return date;
                }
            });
        </script>
        <script>
            $(document).ready(function () {
                $("#returnDatehide").hide();
                $('input:radio[name=type]').change(function () {
                    if (this.value == '1') {
                        $("#returnDatehide").hide();
                    } else if (this.value == '2') {
                        $("#returnDatehide").show();
                    }
                });
            });
        </script>
    </head>

    <body>
        <header>
            <div class="container tv-header">
                <div class="row tv-topbar hidden-sm-down">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="">Giá rẻ hơn khi đặt trên ứng dụng! Tải ngay!</a>
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="row navbar navbar-toggleable-md navbar-light bg-faded header">
                    <div class="container">
                        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <a class="navbar-brand" href="#"><img src="./style/aptechair.png" alt=""></a>
                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Vé máy bay</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Khuyến mại</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="checkorder.html">Tra cứu vé</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <div class="tv-banner">
                <img src="./style/feature.jpg" />
            </div>
        </header>
        <div class="container tv-main">
            <div class="row tv-htable">
                <div class="col-sm-12 txtcenter">
                    <div class="tvSearchTagLineIcon"></div>
                    <h1>Tìm vé chỉ với 3 bước đơn giản!</h1>
                </div>
            </div>
            <s:form method="get" action="timchuyenbay" theme="css_xhtml">
                <div class="row tv-tablesearch">
                    <!-- <div class="col-12">
                    <div class="container ">
                        <div class="row"> -->
                    <div class="col-12 col-md-4">
                        <div class="tv-titlecol">
                            1 <span>Hành trình bay</span>
                        </div>
                        <div class="tv-sitemcol">
                            <span>Điểm khởi hành:</span>
                            <div class="tv-controls">
                                <select id="d1" name="d1">
                                    <optgroup label="Miền Bắc">
                                        <option value="HAN" selected>Hà nội</option>
                                        <option value="HYN">Hưng yên</option>
                                        <option value="HPH">Hải phòng</option>
                                    </optgroup>
                                    <optgroup label="Miền Trung">
                                        <option value="HUI">Huế</option>
                                        <option value="BMV">Buôn Ma Thuật</option>
                                        <option value="CXR">Nha Trang</option>
                                        <option value="VCL">Chu Lai</option>
                                    </optgroup>
                                    <optgroup label="Miền Nam">
                                        <option value="SGN">Hồ Chính Minh</option>
                                        <option value="VCS">Côn Đảo</option>
                                        <option value="CAH">Cà Mau</option>
                                        <option value="PQC">Phú Quốc</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="tv-sitemcol">
                            <div class="tv-hoandoichieubay"></div>
                        </div>
                        <div class="tv-sitemcol">
                            <span>Điểm đến:</span>
                            <div class="tv-controls">
                                <span class="indexVe">
                                    <select id="d2" name="d2">
                                        <optgroup label="Miền Bắc">
                                            <option value="HAN">Hà nội</option>
                                            <option value="HYN">Hưng yên</option>
                                            <option value="HPH">Hải phòng</option>
                                        </optgroup>
                                        <optgroup label="Miền Trung">
                                            <option value="HUI">Huế</option>
                                            <option value="BMV">Buôn Ma Thuật</option>
                                            <option value="CXR">Nha Trang</option>
                                            <option value="VCL">Chu Lai</option>
                                        </optgroup>
                                        <optgroup label="Miền Nam">
                                            <option value="SGN" selected>Hồ Chính Minh</option>
                                            <option value="VCS">Côn Đảo</option>
                                            <option value="CAH">Cà Mau</option>
                                            <option value="PQC">Phú Quốc</option>
                                        </optgroup>
                                    </select>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 tv-cchome">
                        <div class="tv-titlecol">
                            2 <span>Thời gian bay</span>
                        </div>
                        <div class="tv-sitemcol">
                            <span>Ngày đi:</span>
                            <div class="tv-controls">
                                <input type="text" id="flightDate" name="date" class="tv-input-field tv-date-field tv-second-input-field fs-second-col-input" />
                            </div>
                        </div>
                        <div class="tv-sitemcol">
                            <div class="tv-loaivedi">
                                <label class="custom-control custom-radio col-6">
                                    <input name="type" type="radio" value="1" class="custom-control-input" checked>
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Một chiều</span>
                                </label>
                                <label class="custom-control custom-radio">
                                    <input name="type" type="radio" value="2" class="custom-control-input">
                                    <span class="custom-control-indicator"></span>
                                    <span class="custom-control-description">Khứ hồi</span>
                                </label>
                            </div>
                        </div>
                        <div class="tv-sitemcol" id="returnDatehide">
                            <span>Ngày về:</span>
                            <div class="tv-controls">
                                <input type="text" id="returnDate" name="rdate" class="tv-input-field tv-date-field tv-second-input-field fs-second-col-input" />
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="tv-titlecol">
                            3 <span>Tìm chuyến bay</span>
                        </div>
                        <div class="tv-sitemcol">
                            <span>Số hành khách:</span>
                            <div class="tv-controls">
                                <ul class="row">
                                    <li class="col-4" title="Người lớn: Từ 12 tuổi trở lên">
                                        <div class="adultPassengerIcon passengerIcon"></div>
                                        <select class="dropdown" id="adultPassenger" name="nl">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                        </select>
                                    </li>
                                    <li class="col-4" title="Trẻ em: Từ 2 - 11 tuổi">
                                        <div class="childPassengerIcon passengerIcon"></div>
                                        <select class="dropdown" id="childPassenger" name="te">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>
                                    </li>
                                    <li class="col-4" title="Em bé: dưới 2 tuổi">
                                        <div class="infantPassengerIcon passengerIcon"></div>
                                        <select class="dropdown" id="infantPassenger" name="eb">
                                            <option value="0">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tv-sitemcol">
                            <div class="dieukhoan">Bằng việc đăng ký là bạn đã đồng ý với điều khoản của chúng tôi!</div>
                        </div>
                        <div class="tv-sitemcol">
                            <button id="searchFlightSubmit">
                                <div id="searchIcon">
                                </div>
                                <span>Tìm chuyến bay</span>
                            </button>
                        </div>
                    </div>
                    <!-- </div>
                    </div>
                </div> -->
                </div>
            </s:form>
            <div class="row tv-hlistpay">
                <div class="col-sm-12 txtcenter">
                    <p>Các đối tác thanh toán chính thức</p>
                    <div class="listPaymentsLogo">
                        <div class="payment-big-logo"><img src="https://da8hvrloj7e7d.cloudfront.net/imageResource/2016/06/03/1464934417888-cde940f46323c49a4edc12e0b3086b3d.png" alt="OnePay"></div>
                        <div class="payment-big-logo"><img id="payooLogoWithTooltip" src="https://da8hvrloj7e7d.cloudfront.net/imageResource/2016/06/22/1466582990785-244c9487257047f056a3628dff7523cd.png" alt="Payoo" aria-describedby="ui-tooltip-3"></div>
                        <div class="payment-big-logo"><img src="/style/img/vietcombank.png" alt="VietcomBank"></div>
                        <div class="payment-big-logo"><img src="https://da8hvrloj7e7d.cloudfront.net/imageResource/2016/08/08/1470648133167-f3b4c78ccea177d1ccd8f8d938cf0fdd.png" alt="VietinBank"></div>
                        <div class="payment-big-logo secure"><img src="https://d2uoadfmtnmth9.cloudfront.net/v1/lib/images/desktop/assets/logo/rapid-ssl-seal-76e735b7410e7d081b1b19237231c3271.gif" alt="Verisign" class="footer-verisign"></div>
                        <div class="payment-big-logo secure"><img src="https://d2uoadfmtnmth9.cloudfront.net/v1/lib/images/desktop/assets/logo/verisign-669e5aa2863ecfd59dc0f2d9739cb5401.png" alt="RapidSSL Seal" class="footer-ssl"></div>
                    </div>
                </div>
            </div>
            <div class="row tv-hwhy">
                <div class="col-sm-12 txtcenter">
                    <div class="whyWeBookTraveloka"><span><h2>Tại sao nên đặt vé máy bay tại AptechAir?</h2></span></div>
                </div>
                <div class="col-sm-12 txtcenter">
                    <div class="row travelokaCheapExpContainer">
                        <div class="col-12 col-md-4">
                            <div class="travelokaBestAllRoundConvenience travelokaCheapIcon"></div>
                            <div class="textContainer">
                                <h3>Giá cuối cùng</h3>
                                <p>Tất cả giá hiển thị đều là giá cuối cùng. Không thu thêm thuế hay các chi phí ẩn</p>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="travelokaBestPrice travelokaCheapIcon"></div>
                            <div class="textContainer">
                                <h3>Dịch vụ tin cậy</h3>
                                <p>Hỗ trợ khách hàng 24/7. Thanh toán đa dạng. Bảo mật &amp; an toàn tuyệt đối</p>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="travelokaHonestPrice travelokaCheapIcon"></div>
                            <div class="textContainer">
                                <h3>Dễ sử dụng</h3>
                                <p>Giao diện thân thiện. Đặt chỗ nhanh chóng, dễ dàng chỉ trong chưa đầy 1 phút</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footerhome">
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="tv-footerbaomat">
                            <div class="footer-left"><p>Bảo mật AptechAir</p><img src="https://d2uoadfmtnmth9.cloudfront.net/v1/lib/images/desktop/assets/logo/verisign-ef6f7228171e3abf6226f8e99e9423521.png" alt="Verisign" class="footer-verisign"><br><img src="https://d2uoadfmtnmth9.cloudfront.net/v1/lib/images/desktop/assets/logo/rapid-ssl-seal-76e735b7410e7d081b1b19237231c3271.gif" alt="RapidSSL Seal" class="footer-ssl"></div>
                        </div>
                        <div class="col tv-footerlink">
                            <div class="row">
                                <div class="col-6 col-md-4 tv-footeritem">
                                    <h5>Về AptechAir</h5>
                                    <ul><li class="head">Về AptechAir</li><li><a href="https://www.traveloka.com/vi-vn/howto" rel="nofollow">Cách đặt chỗ</a></li><li><a href="https://www.traveloka.com/vi-vn/contactus" rel="nofollow">Liên hệ chúng tôi</a></li><li><a href="https://www.traveloka.com/vi-vn/faq" rel="nofollow">Trợ giúp</a></li><li><a href="https://www.traveloka.com/vi-vn/careers" rel="nofollow">Tuyển dụng</a></li></ul>
                                </div>
                                <div class="col-6 col-md-4 tv-footeritem">
                                    <h5>Câu hỏi thường gặp</h5>
                                    <ul><li class="head">Về AptechAir</li><li><a href="https://www.traveloka.com/vi-vn/howto" rel="nofollow">Cách đặt chỗ</a></li><li><a href="https://www.traveloka.com/vi-vn/contactus" rel="nofollow">Liên hệ chúng tôi</a></li><li><a href="https://www.traveloka.com/vi-vn/faq" rel="nofollow">Trợ giúp</a></li><li><a href="https://www.traveloka.com/vi-vn/careers" rel="nofollow">Tuyển dụng</a></li></ul>
                                </div>
                                <div class="col-12 col-md-4 tv-footeritem">
                                    <div class="footer-cs"><div class="footer-empty-24-hour"><div class="hour">giờ</div></div>
                                        <h5>Hỗ trợ Khách hàng</h5>
                                        <p class="footer-cs-telephone">1900-6978</p></div>
                                    <div class="footer-social"><a href="https://www.facebook.com/TravelokaVN" title="Traveloka trên Facebook" rel="nofollow"><div class="tvContactIcon tvContactFBIcon"></div></a><a href="https://plus.google.com/+Traveloka" title="Traveloka trên Google Plus" rel="nofollow"><div class="tvContactIcon tvContactGPlusIcon"></div></a><a href="https://twitter.com/TravelokaVN" title="Traveloka trên Twitter" rel="nofollow"><div class="tvContactIcon tvContactTwitterIcon"></div></a><a href="https://instagram.com/traveloka" title="Traveloka trên Instagram" rel="nofollow"><div class="tvContactIcon tvContactInstagramIcon"></div></a></div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="coppyright">
                    <div class="container">
                        <div class="row">Bản quyền © 2017 AptechAir - Nghiêm cấm mọi hành vi sao chép.
                        </div></div></div>
            </div>
        </footer>
    </body>

</html>