
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Struts 2 pagination using DataTables</title>
        <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.css">
        <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>-->
        <!--        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css"/>
                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css"/>
                <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
                <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
                <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js"></script>-->

        <!--   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
               <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>-->
        <!--    
                <script src="/style/js/bootstrap.js"></script>
            <link rel="stylesheet" href="/style/css/bootstrap.css">-->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css"/>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js"></script>



        <script type="text/javascript">
            // function for fetching user information from database
            function report() {
                $.ajax({
                    type: "GET",
                    url: "listBoss.html",
                    success: function (result) {
                        var tblData = "";

                        $.each(result.listBoss, function () {
                            var vStatus = "";
                            if (this.status == 0) {
                                vStatus = "<button onclick='updateNewRecord(1, this);' class='btn btn-outline-danger'>Chưa thanh toán</button>";
                            } else if (this.status == 1) {
                                vStatus = "<button onclick='updateNewRecord(0, this);' class='btn btn-outline-success'>Đã thanh toán</button>";
                            } else {
                                vStatus = "<button class='btn btn-outline-secondary' disabled>?ã hu?</button>";
                                ;
                            }
                            tblData +=
                                    "<tr><td>" + this.id + "</td>" +
                                    "<td>" + this.fname + "</td>" +
                                    "<td>" + this.lname + "</td>" +
                                    "<td>" + this.email + "</td>" +
                                    "<td>" + this.phone + "</td>" +
                                    "<td>" + vStatus + "</td></tr>";
                        });
                        $("#tbody").html(tblData);
                    },
                    error: function (result) {
                        alert("G?p v?n ?? nào ?ó! Vui lòng t?i l?i.");
                    }
                });
            }
            // function for updating new information into database
            function updateNewRecord(type, that) {
                $.ajax({
                    type: "POST",
                    url: "confirmBoss.html",
                    data: "type=" + type +
                            "&id=" + $(that).parent().prev().prev().prev().prev().prev().text(),
                    success: function () {
                        report();
                    },
                    error: function () {
                        report();
                        alert("L?i s?y ra!");
                    }
                });
            }

        </script>


        <script type="text/javascript">
            $(document).ready(function () {
                report();
                $("#example").DataTable();
            });
        </script>
    </head>
    <body>
        <h2 >Struts 2 pagination using DataTables<br><br></h2>
        <div class="container">
            <table id="example" class="table table-bordered">
                <thead>
                    <tr class="bg-info">
                        <th>id</th>
                        <th>FName</th>
                        <th>Lname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="tbody">
                </tbody>
            </table>
        </div>
        <div class="container" id="updateBlock">
            <div class="modal fade" id="updateModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title">Update New Information</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="uname" id="fname" class="form-control input-sm" placeholder="Fanme">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="udeg" id="lname" class="form-control input-sm" placeholder="Lname">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="uemail" id="email" class="form-control input-sm" placeholder="Email">
                                <input type="hidden" name="id" id="id">
                            </div>
                            <div class="form-group">
                                <input type="text" name="upass" id="phone" class="form-control input-sm" placeholder="Phone">
                            </div>
                            <button onclick="updateNewRecord();" class="btn btn-info btn-block">Update</button>
                            <div id="resp" class="text-center" style="margin-top: 13px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>