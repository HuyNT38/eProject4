
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Struts 2 pagination using DataTables</title>
        <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.css">
        <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>-->
       <link href="http://jtable.org/Content/themes/flick/jquery-ui-1.10.1.custom.min.css" rel="stylesheet" type="text/css" />
        <link href="http://jtable.org/Scripts/jtable/themes/jqueryui/jtable_jqueryui.css" rel="stylesheet" type="text/css" />
   <link href="http://jtable.org/Scripts/syntaxhighligher/styles/shThemeDefault.css" rel="stylesheet" type="text/css" />

<script src="http://jtable.org/Scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="http://jtable.org/Scripts/jquery-ui-1.10.0.min.js" type="text/javascript"></script>
<script src="http://jtable.org/Scripts/jtable/jquery.jtable.js" type="text/javascript"></script>

        <script type="text/javascript">
 
                $(document).ready(function () {
 
                        $('#StudentTableContainer').jtable({
                                title: 'The Student List',
                                paging: true, //Enable paging
                                pageSize: 10, //Set page size (default: 10)
                                sorting: true, //Enable sorting
                                defaultSorting: 'Name ASC', //Set default sorting
                                actions: {
                                        listAction: 'listBoss.html',
                                        deleteAction: '/Demo/DeleteStudent',
                                        updateAction: '/Demo/UpdateStudent',
                                        createAction: '/Demo/CreateStudent'
                                },
                                fields: {
                                        id: {
                                                key: true,
                                                create: false,
                                                edit: false,
                                                list: false
                                        },
                                        fname: {
                                                title: 'Name',
                                                width: '23%'
                                        },
                                        email: {
                                                title: 'Email address',
                                                list: false
                                        },
                                        phone: {
                                                title: 'User Password',
                                                list: false
                                        }
                                }
                        });
 
                        //Load student list from server
                        $('#StudentTableContainer').jtable('load');
                });
 
        </script>
    </head>
    <body>
        <h2 >Struts 2 pagination using DataTables<br><br></h2>
        <div id="StudentTableContainer"></div>
    </body>
</html>