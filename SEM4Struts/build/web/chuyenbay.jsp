<%-- 
    Document   : chuyenbay
    Created on : Jul 30, 2017, 1:15:31 AM
    Author     : letiep97
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<fmt:requestEncoding value="UTF-8" />
<jsp:useBean id="myBean" class="model.ChuyenBayModel" scope="session" />
<%@taglib prefix="t" uri="/WEB-INF/tlds/customTag.tld" %>
<%@page trimDirectiveWhitespaces="true" %>
<t:header price="0" title="Chuyến bay dành cho bạn"/>
<div class="container tv-main-detail">
    <t:probar num="1"/>
    <div class="row">
        <div class="col-sm-12">
            <h1 class="tv-chonchuyen">
                ${ddName} đến ${ddName2}
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h3 class="tv-khoihanh">
                Khởi hành:
            </h3>
            <span class="tv-ngaydi">
                <img src="./style/carrier-icon.svg" alt="">
                ${ddName} đến ${ddName2} - ${dateFull}
            </span>
        </div>
    </div>
    <div class="row tv-thoigianbay">
        <div class="col-sm-12">
            <span class="tv-prev">
                <a href="?d1=${param.d1}&d2=${param.d2}&date=${btnPrev}&nl=${param.nl}&te=${param.te}&eb=${param.eb}">
                    <img src="/style/icon-round-arrow-left-blue.svg">
                </a>
            </span>
            <ul>
                <c:forEach var="p" items="${myBean.findDayOfWeekFix(param.d1, param.d2, param.date, param.nl, param.te)}">
                    <a href="?d1=${param.d1}&d2=${param.d2}&date=${p.url}&nl=${param.nl}&te=${param.te}&eb=${param.eb}">
                        <li>
                            <div class="tv-lingay">
                                <div class="son-date">${p.dayS}</div>
                                <div class="son-day">${p.dateS}</div>
                            </div>
                            <div class="tv-ligiangay">
                                <c:if test="${p.price!=0}">
                                    <fmt:formatNumber type="number" value="${p.price}" /> <span>VND</span>
                                </c:if>
                                <c:if test="${p.price==0}">
                                    -
                                </c:if>
                            </div>
                        </li>
                    </a>
                </c:forEach>
            </ul>
            <span class="tv-next">
                <a href="?d1=${param.d1}&d2=${param.d2}&date=${btnNext}&nl=${param.nl}&te=${param.te}&eb=${param.eb}">
                    <img src="/style/icon-round-arrow-right-blue.svg" alt="">
                </a>
            </span>
        </div>
    </div>
    <div class="row tv-chuyenbay">
        <div class="col-sm-12">
            <div class="row tv-subchuyentien">
                <div class="col-9 tv-datechuyenbay">
                    ${dateFull}
                </div>
                <div class="col-3 tv-typeprice text-right">
                    Việt Nam Đồng
                </div>
            </div>
        </div>
        <div class="col-sm-12">
            <c:if test="${myBean.findAir(param.d1, param.d2, param.date, param.nl, param.te).size()<=0 }">
                <div class="tv-errchuyenbay text-center">
                    <img class="" src="./style/emty-chuyen-bay.png" alt="Flight not available">
                    <h1>Không có chuyến bay</h1>
                    <p>Vui lòng chọn ngày bay khác</p>
                </div>
            </c:if>
            <c:if test="${myBean.findAir(param.d1, param.d2, param.date, param.nl, param.te).size()>0 }">
                <c:forEach var="p" items="${myBean.findAir(param.d1, param.d2, param.date, param.nl, param.te)}">
                    <c:if test="${p.type==2||p.type==4}">
                        <a href="nhapthongtin.html?id=${p.id}&nl=${param.nl}&te=${param.te}&eb=${param.eb}">
                            <div class="tv-ttchuyenbay">
                            </c:if>
                            <c:if test="${p.type==1||p.type==3}">
                                <a>
                                    <div class="tv-ttchuyenbay tv-hetve">
                                    </c:if>
                                    <div class="row">
                                        <div class="col-6 col-sm-4 col-md-4 col-lg-3 tv-khungthoigian">
                                            <div class="tv-giodi">
                                                ${p.timeDi}
                                                <span>${p.DDi} - Đi</span>
                                            </div>
                                            <span class="tv-iconbay"><img src="https://booking.jetstar.com/Images/Icons/Icon_carrier_1_16x14.svg" alt=""></span>
                                            <div class="tv-gioden">
                                                ${p.timeDen}
                                                <span>${p.DDd} - Đến</span>
                                            </div>
                                        </div>
                                        <div class="col-6 col-sm-8 col-md-8 col-lg-9">
                                            <div class="row">
                                                <div class="col hidden-md-down tv-khoangthoigian">
                                                    Bay thẳng - khoảng ${p.timeBay}
                                                </div>
                                                <c:if test="${p.type==2}">
                                                    <div class="col tv-giachuyenbay">
                                                        <fmt:formatNumber type="number" value="${p.price}" />
                                                        <span>VND</span>
                                                        <input type="button" value="Chọn" class="hidden-md-down tv-chonchuyen">
                                                    </div>
                                                </c:if>
                                                <c:if test="${p.type==4}">
                                                    <div class="col tv-giachuyenbay tv-b">
                                                        <fmt:formatNumber type="number" value="${p.price}" />
                                                        <span>VND</span>
                                                        <input type="button" value="Chọn" class="hidden-md-down tv-chonchuyen">
                                                    </div>
                                                </c:if>
                                                <c:if test="${p.type==1}">
                                                    <div class="col tv-giachuyenbay">
                                                        Chuyến bay hết vé
                                                        <input type="button" value="Chọn" class="hidden-md-down tv-chonchuyen">
                                                    </div>
                                                </c:if>
                                                <c:if test="${p.type==3}">
                                                    <div class="col tv-giachuyenbay">
                                                        Không đủ ghế
                                                        <input type="button" value="Chọn" class="hidden-md-down tv-chonchuyen">
                                                    </div>
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </c:forEach>
                    </c:if>
                </div>
        </div>
    </div>
</div>
<s:include value="footer.jsp" />