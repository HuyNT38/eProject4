<%-- 
    Document   : nhapthongtin
    Created on : Aug 14, 2017, 11:54:14 AM
    Author     : letiep97
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<fmt:requestEncoding value="UTF-8" />
<%@taglib prefix="t" uri="/WEB-INF/tlds/customTag.tld" %>
<t:header price="${(ttchuyendi.nl+ttchuyendi.te)*ttchuyendi.price+ttchuyendi.eb*ttchuyendi.priceeb}"
          title="Nhập thông tin quý khách"/>
<script type="text/javascript">
    $(function () {
        // var mon = $('#input_1');
        // mon.keyup(function(){
        //     $('#content_1').text(''+mon.val());
        // })
        $(".input").keyup(function () {
            var curId = this.id.split("_")[1];
            $("#content_" + curId).html($(this).val() + " " + $("#input2_" + curId).val());
        });
        $(".input2").keyup(function () {
            var curId = this.id.split("_")[1];
            $("#content_" + curId).html($("#input_" + curId).val() + " " + $(this).val());
        });
    })
</script>

<s:form class="form-horizontal" method="post" action="datcho" theme="css_xhtml">
    <div class="container tv-main-detail">

        <div class="tv-nhapthongtin">
        <t:probar num="2"/>
        <div class="row">
                <div class="col-sm-12 txtcenter">
                    <h1 class="tv-chonchuyen">
                        Thông tin hàng khách
                    </h1>
                    <p class="tv-desc">Hãy điền đầy đủ thông tin của quý khách để đam bảo quyền lợi của mình</p>
                </div>
            </div>

            <input name="id" value="${param.id}" type="hidden">
            <input name="nl" value="${param.nl}" type="hidden">
            <input name="te" value="${param.te}" type="hidden">
            <input name="eb" value="${param.eb}" type="hidden">
            <c:set var="count" value="1" scope="page" />
            <c:forEach var = "i" begin = "1" end = "${param.nl}">
                <input name="nlid${i}" value="${count}" type="hidden">
                <div class="tv-accinfo">
                    <h4>Người lớn ${i}</h4>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <label for="">Họ và tên đệm</label>
                            <input name="nlfname${i}" id="input_${i}" class="input" type="text" placeholder="Họ và tên đệm" autocomplete="off">
                        </div>
                        <div class="col-12 col-md-5">
                            <label for="">Tên</label>
                            <input name="nllname${i}" id="input2_${i}" type="text" class="input2" placeholder="Tên" autocomplete="off">
                        </div>
                        <div class="col-12 col-md-3">
                            <label for="">Giới tính</label>
                            <select name="nlgender${i}">
                                <!--                            <option>Giới tính</option>-->
                                <option value="1">Nam</option>
                                <option value="2">Nữ</option>
                            </select>
                        </div>
                    </div>
                </div>
                <c:set var="count" value="${count + 1}" scope="page"/>
            </c:forEach>

            <c:forEach var = "i" begin = "1" end = "${param.te}">
                <input name="teid${i}" value="${count}" type="hidden">
                <div class="tv-accinfo">
                    <h4>Trẻ em ${i}</h4>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <label for="">Họ và tên đệm</label>
                            <input name="tefname${i}" type="text" placeholder="Họ và tên đệm" autocomplete="off">
                        </div>
                        <div class="col-12 col-md-5">
                            <label for="">Tên</label>
                            <input name="telname${i}" type="text" placeholder="Tên" autocomplete="off">
                        </div>
                        <div class="col-12 col-md-3">
                            <label for="">Giới tính</label>
                            <select name="tegender${i}">
                                <!--<option>Giới tính</option>-->
                                <option value="1">Nam</option>
                                <option value="2">Nữ</option>
                            </select>
                        </div>
                    </div>
                </div>
                <c:set var="count" value="${count + 1}" scope="page"/>
            </c:forEach>

            <c:forEach var = "i" begin = "1" end = "${param.eb}">
                <input name="ebid${i}" value="${count}" type="hidden">
                <div class="tv-accinfo">
                    <h4>Em bé ${i}</h4>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <label for="">Họ và tên đệm</label>
                            <input name="ebfname${i}" type="text" placeholder="Họ và tên đệm" autocomplete="off">
                        </div>
                        <div class="col-12 col-md-5">
                            <label for="">Tên</label>
                            <input name="eblname${i}" type="text" placeholder="Tên" autocomplete="off">
                        </div>
                        <div class="col-12 col-md-3">
                            <label for="">Giới tính</label>
                            <select name="ebgender${i}">
                                <!--<option>Giới tính</option>-->
                                <option value="1">Nam</option>
                                <option value="2">Nữ</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 col-md-2">
                            <label for="">Ngày</label>
                            <select name="ebday${i}">
                                <option>Ngày</option>
                                <c:forEach var = "n" begin = "1" end = "31">
                                    <option value="${n}">${n}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-6 col-md-3">
                            <label for="">Tháng</label>
                            <select name="ebmonth${i}">
                                <option>Tháng</option>
                                <c:forEach var = "a" begin = "1" end = "12">
                                    <option value="${a}">Tháng ${a}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-6 col-md-3">
                            <label for="">Năm</label>
                            <select name="ebyear${i}">
                                <option>Năm</option>
                                <option value="2015">2015</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                            </select>
                        </div>
                        <div class="col-6 col-md-4">
                            <label for="">Đi cùng với</label>
                            <select name="ebwith${i}">
                                <c:forEach var = "b" begin = "1" end ="${param.nl}">
                                    <option value="${b}" id="content_${b}">Người lớn ${b}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                <c:set var="count" value="${count + 1}" scope="page"/>
            </c:forEach>
            <div class="tv-accinfo">
                <h3>Thông tin liên hệ</h3>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <label for="">Họ và tên đệm</label>
                        <input name="fname" type="text" placeholder="Họ và tên đệm" autocomplete="off">
                    </div>
                    <div class="col-12 col-md-6">
                        <label for="">Tên</label>
                        <input name="lname" type="text" placeholder="Tên" autocomplete="off">
                    </div>
                    <div class="col-12 col-md-6">
                        <label for="">Email</label>
                        <input name="email" type="email" placeholder="Thư điện tử" autocomplete="off">
                    </div>
                    <div class="col-12 col-md-6">
                        <label for="">Số điện thoại</label>
                        <input name="phone" type="number" placeholder="Số điện thoại" autocomplete="off">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tv-barfooter">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <s:submit value="Tiếp tục tới đặt chỗ" cssClass="tv-chon"/>
                </div>
            </div>
        </div>
    </div>
</s:form>
<s:include value="footer.jsp" />