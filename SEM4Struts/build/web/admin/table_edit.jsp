
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Struts 2 pagination using DataTables</title>
        <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.css">
        <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>-->
        <!--        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css"/>
                <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css"/>
                <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
                <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
                <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js"></script>-->
 <link rel="stylesheet" type="text/css" href="../media/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="../media/css/dataTables.editor.css">
<link rel="stylesheet" type="text/css" href="../media/extensions/Buttons/css/buttons.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="../media/extensions/Select/css/select.dataTables.min.css">
<script type="text/javascript" charset="utf-8" src="../media/js/jquery.js"></script>
<script type="text/javascript" charset="utf-8" src="../media/js/jquery.dataTables.js"></script>		
<script type="text/javascript" charset="utf-8" src="http://jed-datatables.net/media/js/dataTables.editor.min.js"></script> 
<script type="text/javascript" charset="utf-8" src="../media/extensions/Buttons/js/dataTables.buttons.min.js"></script> 
<script type="text/javascript" charset="utf-8" src="../media/extensions/Select/js/dataTables.select.min.js"></script>   
     


        <script type="text/javascript">



            var editor; // use a global for the submit and return data rendering in the examples

            $(document).ready(function () {
                editor = new $.fn.dataTable.Editor({
                    ajax: "listBoss.html",
                    table: "#example",
                    fields: [{
                            label: "Browser:",
                            name: "fname"
                        }, {
                            label: "Rendering engine:",
                            name: "fname"
                        }, {
                            label: "Platform:",
                            name: "fname"
                        }, {
                            label: "Version:",
                            name: "fname"
                        }, {
                            label: "CSS grade:",
                            name: "fname"
                        }
                    ]
                });

                $('#example').DataTable({
                    dom: "Bfrtip",
                    ajax: "listBoss.html",
                    columns: [
                        {data: "fname"},
                        {data: "fname"},
                        {data: "fname"},
                        {data: "fname", className: "ColumnAlignCenter"},
                        {data: "fname", className: "ColumnAlignCenter"}
                    ],
                    select: true,
                    buttons: [
                        {extend: "create", editor: editor},
                        {extend: "edit", editor: editor},
                        {extend: "remove", editor: editor}
                    ]
                });
            });



        </script>
    </head>
    <body>
        <h2 >Struts 2 pagination using DataTables<br><br></h2>

        <table class="display" id="example">
            <thead>
                <tr>
                    <th width="25%">Browser</th>
                    <th width="20%">Rendering engine</th>			  
                    <th width="25%">Platform(s)</th>
                    <th width="15%">Engine version</th>
                    <th width="15%">CSS grade</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="5" class="dataTables_empty">Loading data from server</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th>Browser</th>
                    <th>Rendering engine</th>			    
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade</th>
                </tr>
            </tfoot>
        </table>




    </body>
</html>