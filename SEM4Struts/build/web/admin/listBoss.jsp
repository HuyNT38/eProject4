<%-- 
    Document   : listBoss
    Created on : Aug 23, 2017, 3:23:53 PM
    Author     : letiep97
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    
        <script src="/style/js/bootstrap.js"></script>
    <link rel="stylesheet" href="/style/css/bootstrap.css">
        <script type="text/javascript">
            // function for fetching user information from database
            function report() {
                $.ajax({
                    type: "GET",
                    url: "listBoss.html",
                    success: function (result) {
                        var tblData = "";

                        $.each(result.listBoss, function () {
                            var vStatus = "";
                            if (this.status == 0) {
                                vStatus = "<button onclick='updateNewRecord(1, this);' class='btn btn-outline-danger'>Chưa thanh toán</button>";
                            } else if (this.status == 1) {
                                vStatus = "<button onclick='updateNewRecord(0, this);' class='btn btn-outline-success'>Đã thanh toán</button>";
                            } else {
                                vStatus = "<button class='btn btn-outline-secondary' disabled>Đã huỷ</button>";
                                ;
                            }
                            tblData +=
                                    "<tr><td>" + this.id + "</td>" +
                                    "<td>" + this.fname + "</td>" +
                                    "<td>" + this.lname + "</td>" +
                                    "<td>" + this.email + "</td>" +
                                    "<td>" + this.phone + "</td>" +
//                                    "<td>" + this.status + "</td>" +
                                    "<td>" + vStatus + "</td></tr>";
                        });
                        $("#tbody").html(tblData);
                    },
                    error: function (result) {
                        alert("Gặp vấn đề nào đó! Vui lòng tải lại.");
                    }
                });
            }

            // function for fecthing old information into the form
//            function fetchOldRecord(that) {
//                $("#resp").html("");
//                $("#fname").val($(that).parent().prev().prev().prev().prev().prev().text());
//                $("#lname").val($(that).parent().prev().prev().prev().prev().text());
//                $("#email").val($(that).parent().prev().prev().prev().text());
//                $("#phone").val($(that).parent().prev().prev().text());
//                $("#id").val($(that).parent().prev().prev().prev().prev().prev().prev().text());
//            }

            // function for updating new information into database
            function updateNewRecord(type, that) {
                $.ajax({
                    type: "POST",
                    url: "confirmBoss.html",
                    data: "type=" + type +
                            "&id=" + $(that).parent().prev().prev().prev().prev().prev().text(),
                    success: function () {
//                        $("#resp").html("Thanh con!");
                        report();
                    },
                    error: function () {
                        report();
                        alert("Lỗi sảy ra!");
                    }
                });
            }

        </script>
    </head>
    <body onload="report();">
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="/">CRUD</a>
                </div>
                <ul class="nav navbar-nav">
                    <li>
                        <a href="index.jsp">Register</a>
                    </li>
                    <li>
                        <a href="report.jsp">Report</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <table class="table table-bordered">
                <thead>
                    <tr class="bg-info">
                        <th>id</th>
                        <th>FName</th>
                        <th>Lname</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody id="tbody">
                </tbody>
            </table>
        </div>
        <div class="container" id="updateBlock">
            <div class="modal fade" id="updateModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title">Update New Information</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="uname" id="fname" class="form-control input-sm" placeholder="Fanme">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <input type="text" name="udeg" id="lname" class="form-control input-sm" placeholder="Lname">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="uemail" id="email" class="form-control input-sm" placeholder="Email">
                                <input type="hidden" name="id" id="id">
                            </div>
                            <div class="form-group">
                                <input type="text" name="upass" id="phone" class="form-control input-sm" placeholder="Phone">
                            </div>
                            <button onclick="updateNewRecord();" class="btn btn-info btn-block">Update</button>
                            <div id="resp" class="text-center" style="margin-top: 13px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>