
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="s" uri="/struts-tags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>AptechAir</title>
        <!--<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.css">
        <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>-->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.4.0/css/buttons.bootstrap4.min.css"/>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js"></script>

        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.0/js/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.bootstrap4.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.html5.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.print.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.0/js/buttons.colVis.min.js"></script>



        <script type="text/javascript">

            $(document).ready(function () {

                var table = $("#example").DataTable({
                    ajax: {
                        url: 'listBoss.html',
                        data: function (data) {
                            delete data.columns;
                        }
                    },
                    columns: [
                        {"data": "id",
                            "render": function (data, type, mete) {
                                return pad(data, 9);
                            }
                        },
                        {data: null, render: function (data, type, row) {
                                return data.fname + ' ' + data.lname;
                            }},
                        {"data": "email"},
                        {"data": "phone"},
                        {data: 'price',
//                            render: $.fn.dataTable.render.number('.', '.', 0, '') },
                            render: function (data, type, row) {
                                return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " <span>VND</span>";
                            }},
                        {"data": "status",
                            "render": function (data, type, meta) {
                                if (data == 0) {
                                    return "<button onclick='updateNewRecord(1, this)' class='btn btn-outline-danger'>Chưa thanh toán</button>";
                                }
                                if (data == 1) {
                                    return "<button onclick='updateNewRecord(0, this)' class='btn btn-outline-success'>Đã thanh toán</button>";

                                } else {
                                    return "<button class='btn btn-outline-secondary' disabled>Đã huỷ</button>";
                                }
                            }
                        }

                    ],
//                    scrollY: "500px",
//                    scrollX: "100%",
//                    
//                    lengthChange: false,
                    "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "Tất cả"]],
//        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
//                    dom: 'Bfrtip',
//                    buttons: [
//                        {
//                            text: 'My button',
////                            action: function (e, dt, node, config) {
////                                alert('Button activated');
////                            }
//                        }
//                    ],
                    "columnDefs": [
//                        {
//                            "targets": 5,
//                            "orderable": false
//                        },
                        {"targets": 4,
                            "orderable": false},
                        {"targets": 3,
                            "orderable": false},
                    ],
                    order: [[0, "desc"]],
                    "language": {
                        "search": "Nhập Code/Tên/Email/Phone:",
                        lengthMenu: "Hiện _MENU_ yêu cầu",
//                        info: "Đang ở trang _PAGE_ trên _PAGES_",
                        info: "",
                        "paginate": {
                            "next": ">",
                              "previous": "<"
                        },
                          "loadingRecords": "Đang tải vui lòng chờ..."
                    },
                     dom: 'Bfrtip',
        buttons: [
            {
                extend: 'print',
                message: 'This print was produced using the Print button for DataTables'
            },
            // dom:' <"search"f><"top"l>rt<"bottom"ip><"clear">',
        ]

                });

                // table.buttons().container().appendTo('#example_wrapper .col-md-6:eq(0)');

                setInterval(function () {
                    table.ajax.reload(null, false); // user paging is not reset on reload
                }, 5000);
            });

            function updateNewRecord(type, that) {
                $.ajax({
                    type: "POST",
                    url: "confirmBoss.html",
                    data: "type=" + type +
                            "&id=" + $(that).parent().prev().prev().prev().prev().prev().text(),
                    success: function () {
                        if (type == 1) {
                            $(that).attr("onclick", "updateNewRecord(0, this)");
                            $(that).removeClass("btn-outline-danger");
                            $(that).addClass("btn-outline-success");
                            $(that).text("Đã thanh toán");
                        } else {
                            $(that).attr("onclick", "updateNewRecord(1, this)");
                            $(that).addClass("btn-outline-danger");
                            $(that).removeClass("btn-outline-success");
                            $(that).text("Chưa thanh toán");
                        }
                    },
                    error: function () {
                        report();
                        alert("Lỗi sảy ra!");
                    }
                });
            }
            function pad(str, max) {
                str = str.toString();
                return str.length < max ? pad("0" + str, max) : str;
            }

        </script>
        <style>#example span {
                font-size: 50%;
            }</style>
    </head>
    <body>
        <h2>ahjhj</h2>
        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Code</th>
                    <th>Họ và Tên</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Số tiền</th>
                    <th>Trạng thái</th>
                </tr>
            </thead>       
        </table>
    </body>
</html>