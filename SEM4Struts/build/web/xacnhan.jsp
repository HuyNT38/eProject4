<%-- 
    Document   : datcho
    Created on : Aug 15, 2017, 12:33:24 AM
    Author     : letiep97
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<fmt:requestEncoding value="UTF-8" />
<jsp:useBean id="myBean" class="model.ChuyenBayModel" scope="session" />
<%@taglib prefix="t" uri="/WEB-INF/tlds/customTag.tld" %>
<t:header price="${priceSum}" 
          title="Chọn chỗ ngồi bạn thích"/>
<link rel="stylesheet" href="./style/seats.css">

<div class="container tv-main-detail">
    <div class="tv-datcho">
        <t:probar num="4"/>
        <div class="row">
            <div class="col-sm-12 txtcenter">
                <h1 class="tv-chonchuyen">
                    Kiểm tra và thanh toán
                </h1>
                <p class="tv-desc">Hãy chắc chắn thông tin dưới đây đúng yêu theo yêu cầu của bạn trước khi thanh toán.
                </p>
            </div>
        </div>
        <div class="container tv-xacnhan">
            <div class="row tv-hbooking txtcenter">
                <div class="col-12">
                    <h5>${ttchuyendi.ddName} đến ${ttchuyendi.ddName2}
                    </h5>
                    <span>${ttchuyendi.dateFull}</span>
                </div>
            </div>
            <div class="row subbooking">
                ${ttchuyendi.ddName} đến ${ttchuyendi.ddName2} - Bắt đầu lúc ${ttchuyendi.timeDi}
            </div>
            <div class="row tv-booking">
                <div class="tv-iconbooking">
                    <i class="icon--lg icon-fare-primary"></i>
                </div>
                <div class="tv-bookgrow">
                    <h5>Chuyến bay ${ttchuyendi.name}</h5>
                    <div class="tv-bookingitem">
                        <span>${ttchuyendi.nl} x Người lớn</span>
                        <span>
                            <fmt:formatNumber type="number" value="${ttchuyendi.nl*ttchuyendi.price}" /> <i>VND</i>
                        </span>
                    </div>
                    <c:if test="${ttchuyendi.te>=1}">
                        <div class="tv-bookingitem">
                            <span>${ttchuyendi.te} x Trẻ em</span>
                            <span>
                                <fmt:formatNumber type="number" value="${ttchuyendi.te*ttchuyendi.price}" /> <i>VND</i>
                            </span>
                        </div>
                    </c:if>
                    <c:if test="${ttchuyendi.eb>=1}">
                        <div class="tv-bookingitem">
                            <span>${ttchuyendi.eb} x Em bé</span>
                            <span><fmt:formatNumber type="number" value="${ttchuyendi.eb*ttchuyendi.priceeb}" /> <i>VND</i></span>
                        </div>
                    </c:if>
                </div>
            </div>
            <div class="row tv-booking">
                <div class="tv-iconbooking">
                    <i class="icon--lg icon-seats-primary"></i>
                </div>
                <div class="tv-bookgrow">
                    <h5>Chỗ ngồi</h5>
                    <c:set var="count" value="0" scope="page" />
                    <c:forEach var="a" items="${listkh}" varStatus="status" >  
                        <c:if test="${a.type==1||a.type==2}">
                            <div class="tv-bookingitem">
                                <span>${a.fname} ${a.lname} - ${a.idG} 
                                    <c:if test="${a.typeG==1}">
                                        (ghế phổ thông)
                                    </c:if>
                                    <c:if test="${a.typeG==3}">
                                        (ghế thương gia)
                                    </c:if>
                                    <c:if test="${a.typeG==2}">
                                        (ghế hạng nhất)
                                    </c:if>
                                </span>
                                <span><fmt:formatNumber type="number" value="${a.priceG}" /> <i>VND</i></span>
                            </div>
                            <c:set var="count" value="${count+a.priceG}" scope="page"/>
                        </c:if>
                    </c:forEach>

                </div>
            </div>
            <div class="row tv-bookings">
                <span>Tổng tiền đặt chỗ</span>
                <span>
                    <fmt:formatNumber type="number" value="${(ttchuyendi.nl+ttchuyendi.te)*ttchuyendi.price+ttchuyendi.eb*ttchuyendi.priceeb+count}" />
                    <i>VND</i>
                </span>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 txtcenter">
                <h1 class="tv-chonchuyen">
                    Hình thức thanh toán
                </h1>
                <p class="tv-desc">Quý khách có thể lựa chọn nhiều hình thức thanh toán.
                </p>
            </div>
        </div>
        <div class="container">
            <div class="row tv-loaithanhtoan txtcenter">
                <div class="col-6">
                    <div class="tv-loaittitem">Thẻ tín dụng</div>
                </div>
                <div class="col-6">
                    <div class="tv-loaittitem tv-pick">Trả sau</div>
                </div>
            </div>
        </div>
    </div>
</div>
<s:form class="form-horizontal" method="post" action="done" theme="css_xhtml">
    <div class="tv-barfooter">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <s:submit value="Xác nhận" cssClass="tv-chon"/>
                </div>
            </div>
        </div>
    </div>
</s:form>
<script>
    $(".tv-chon").click(function () {
        $(".overloadpage").show();
    });
</script>
<div class="overloadpage" style="display: none;">
    <!--<p>Hệ thống đang xử lý...</p>-->
    <div class="spin-loader"></div>
</div>                    

<s:include value="footer.jsp" />