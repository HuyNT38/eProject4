<%-- 
    Document   : datcho
    Created on : Aug 15, 2017, 12:33:24 AM
    Author     : letiep97
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<fmt:requestEncoding value="UTF-8" />
<jsp:useBean id="myBean" class="model.ChuyenBayModel" scope="session" />
<%@taglib prefix="t" uri="/WEB-INF/tlds/customTag.tld" %>
<t:header price="${(ttchuyendi.nl+ttchuyendi.te)*ttchuyendi.price+ttchuyendi.eb*ttchuyendi.priceeb+priceSeat}"
          title="Chọn chỗ ngồi bạn thích"/>
<link rel="stylesheet" href="./style/seats.css">
<%@page trimDirectiveWhitespaces="true" %>

<s:form class="form-horizontal" method="post" action="xacnhan" theme="css_xhtml">
    <div class="container tv-main-detail">
        <div class="tv-datcho">
            <t:probar num="3"/>
            <div class="row">
                <div class="col-sm-12 txtcenter">
                    <h1 class="tv-chonchuyen">
                        Chọn chỗ ngồi của bạn
                    </h1>
                    <p class="tv-desc">Để có chỗ ngồi tốt như ý thích của mình, hãy lựa chọn ngay bây giờ. Nếu bạn không quan tâm, chỗ ngồi sẽ được chọn tự động chọn.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="tv-khoihanh">
                        Chuyến bay ${ttchuyendi.name}:
                    </h3>
                    <span class="tv-ngaydi">
                        <img src="https://booking.jetstar.com/Images/Icons/Icon_carrier_4_16x14.svg">
                        ${ttchuyendi.ddName} đến ${ttchuyendi.ddName2} - ${ttchuyendi.dateFull}
                    </span>
                </div>
            </div>
            <div class="container">
                <div class="tv-chonghes">
                    <div class="row">
                        <div class="col-12 col-md-5 tv-chonghe-left">
                            <div class="tv-chonghe-maybay">
                                <img src="https://booking.jetstar.com/Images/aircraft/minimap/a320.png" alt="">
                                <div class="plane">
                                    <div class="cockpit">
                                        <!--   <h4>Buồng lái</h4> -->
                                    </div>
                                    <div class="exit exit--front fuselage">
                                    </div>
                                    <ol class="cabin fuselage">
                                        <c:set var="alphabet">A,B,C,D,E,F</c:set>
                                        <c:forEach var = "i" begin = "1" end = "12">
                                            <c:choose>
                                                <c:when test="${i==1||i==2}">
                                                    <c:set var="ghevip" value="class='tv-ghevip'" scope="page" />
                                                </c:when>
                                                <c:when test="${i>=3&&i<=4}">
                                                    <c:set var="ghevip" value="class='tv-gheleg'" scope="page" />
                                                </c:when>
                                                <c:otherwise>
                                                    <c:set var="ghevip" value="" scope="page" />
                                                </c:otherwise>
                                            </c:choose>
                                            <li>
                                                <ol class="seats">
                                                    <c:forTokens items="${alphabet}" delims="," var="letter">
                                                        <c:forEach var="list" items="${listSeat}">
                                                            <c:set var="idGhe" value="${i}${letter}" scope="page" />
                                                            <c:if test="${list.id == idGhe}">
                                                                <c:set var="price" value="${list.price}" scope="page" />
                                                            </c:if>
                                                        </c:forEach>
                                                        <li class="seat">
                                                            <input type="checkbox" id="${i}${letter}" price="${price}"/>
                                                            <label for="${i}${letter}" ${ghevip}>${i}${letter}</label>
                                                        </li>
                                                    </c:forTokens>
                                                </ol>
                                            </li>
                                        </c:forEach>
                                    </ol>
                                    <div class="exit exit--back fuselage">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-7 tv-chonghe-right">
                            <div class="tv-ghenote">
                                <div class="tv-ghenote-title">
                                    <h3>Ghi chú</h3>
                                </div>
                                <div class="tv-ghenote-item">
                                    <div class="row">
                                        <span class="col-6">
                                            <li class="seat"><input type="checkbox" disabled><label></label></li>
                                            <div>Đã được đặt</div>
                                        </span>
                                        <span class="col-6">
                                            <li class="seat"><input type="checkbox" checked><label></label></li>
                                            <div>Ghế của bạn</div>
                                        </span>
                                        <span class="col-6">
                                            <li class="seat"><input type="checkbox"><label class="tv-ghevip"></label></li>
                                            <div>Ghế hạng nhất</div>
                                        </span>
                                        <span class="col-6">
                                            <li class="seat"><input type="checkbox"><label class="tv-gheleg"></label></li>
                                            <div>Ghế thương gia</div>
                                        </span>
                                        <span class="col-6">
                                            <li class="seat"><input type="checkbox"><label></label></li>
                                            <div>Ghế phổ thông</div>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="tv-userchon">
                                <div class="tv-userchon-title">
                                    <h3>Ghế ngồi của hành khách</h3>
                                </div>
                                <c:set var="count" value="0" scope="page" />
                                <div class="row tv-userchon-head">
                                    <div class="col-6">
                                        Hàng khách
                                    </div>
                                    <div class="col-2">
                                        Ghế
                                    </div>
                                    <div class="col-4 text-right">
                                        Giá
                                    </div>
                                </div>
                                <c:forEach var="a" items="${listkh}" varStatus="status" > 
                                    <c:if test="${a.type==1||a.type==2}">
                                        <!-- <input type="text" name="idP${a.id}" value="${listSeatAuto.get(count).price}" class="gheprice" hidden/> -->
                                        <input type="text" name="idG${a.id}" value="${listSeatAuto.get(count).id}" price="${listSeatAuto.get(count).price}" class="gheview" hidden/>

                                        <div class="row tv-userchon-item" id="${count+1}">
                                            <div class="col-6">${a.fname}&nbsp;${a.lname}</div>
                                            <div id="ghe${a.id}" class="col-2 gheview"><c:out value="${listSeatAuto.get(count).id}"/></div>
                                            <div class="col-4 text-right">
                                                <span id="pghe${a.id}">
                                                    <fmt:formatNumber type="number" value="${listSeatAuto.get(count).price}" />
                                                </span> <i>VND</i>
                                            </div>
                                        </div>

                                        <c:set var="count" value="${count + 1}" scope="page"/>
                                    </c:if>
                                </c:forEach>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tv-barfooter">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <s:submit value="Thanh toán" cssClass="tv-chon"/>
                </div>
            </div>
        </div>
    </div>
</s:form>


<script type="text/javascript">
    var i = 1;
    var price = 0;
    var count = 0;

    $('.tv-userchon-item#' + i).addClass("tv-ghe-pointer");

    $(".ghe" + i).css("background-color", "red");
    $("input:checkbox").click(function () {
        count = 0;
        var ghe = $('input[name=idG' + i + ']').val();// Get ghe id 1
        $("input[id='" + ghe + "']").prop("checked", false);//Bỏ chọn

        var ghechon = this.id;
        $('input[name=idG' + i + ']').attr("price", $(this).attr("price"));
        $("#pghe" + i).text($(this).attr("price").replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));// Set price
        $('.gheview').each(function () {
            ++count;
            if ($(this).val() == ghechon || $(this).text() == ghechon) {
                // $(this).val("");
                $(this).attr("value", "");
                $(this).text("");
                $(this).attr("price", "0");
                $("#pghe" + this.id.substr(3, 1)).text("0");// Set price
            }
            if ($(this).attr("price") > 0) {
                price = price + parseInt($(this).attr("price"));
            }

        });// Chọn ghế người khác

        if (count == 2) {///Nếu chỉ có 1 ghế thì get lại giá cũ
            $("#pghe" + i).text($(this).attr("price").replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));// Set price
        }
        // alert(price);
        $('.tv-cartheaditem').html(
                (${(ttchuyendi.nl+ttchuyendi.te)*ttchuyendi.price+ttchuyendi.eb*ttchuyendi.priceeb} + price)
                .toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + "<span>VND</span>");
        price = 0;

        //// $('input[name=idG' + i + ']').val(ghechon);// Set value = this.id
        $('input[name=idG' + i + ']').attr("value", ghechon);//// Fix cho dòng trên... chưa hiểu lý do
        $("#ghe" + i).text(ghechon);// Set text view



        $(this).prop('checked', true);//Nếu đã check rồi

        $('.tv-userchon-item').each(function () {
            $(this).removeClass("tv-ghe-pointer");
        });


        ++i;
        //$("#pghe" + i).css("background-color", "red");
        if (i >${ttchuyendi.nl + ttchuyendi.te}) {
            i = 1;
        }
        $('.tv-userchon-item#' + i).addClass("tv-ghe-pointer");
    });

    $(".tv-userchon-item").click(function () {
        var index = $(".tv-userchon-item").index(this);
        i = index + 1;
        $('.tv-userchon-item').each(function () {
            $(this).removeClass("tv-ghe-pointer");
        });
        $(this).addClass("tv-ghe-pointer");
    });

    // $("input:checkbox").click(function () {
    //     var bol = $("input:checkbox:checked").length > ${ttchuyendi.nl + ttchuyendi.te};
    //     if (bol) {
    //         alert('Không thể chọn số ghế nhiều hơn số lượng người.');
    //         $(this).prop('checked', false);
    //     }
    // });

    <c:forEach var="a" items="${listSeatAuto}" varStatus="status" >
    $("input[id='${a.id}']").prop("checked", true);
    </c:forEach>
    <s:iterator value="listSeatChose" status="statusVar">
    $("input[id='<s:property/>']").prop("disabled", true);
    </s:iterator>

</script>
<s:include value="footer.jsp" />