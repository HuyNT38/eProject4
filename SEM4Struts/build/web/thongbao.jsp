<%-- 
    Document   : datcho
    Created on : Aug 15, 2017, 12:33:24 AM
    Author     : letiep97
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<fmt:requestEncoding value="UTF-8" />
<%@taglib prefix="t" uri="/WEB-INF/tlds/customTag.tld" %>
<t:header price="0" 
          title="Đặt vé thành công"/>

<div class="container tv-main-detail">
    <div class="tv-datcho">
<!--        <div class="row">
            <div class="col-sm-12 txtcenter">
                <h1 class="tv-chonchuyen">
                    Thàng công!<br/><br/>
                    Mã đơn hàng của bạn là: <span style="color: green"> ${idBoss}</span>
                </h1>
                <p class="tv-desc">Vui lòng kiểm tra mail.
                </p>
            </div>
        </div>-->
        <div class="container tv-xacnhan tv-xacnhan-margin">
            <div class="row tv-hbooking txtcenter">
                <div class="col-12">
                    <div><img src="/style/thongbao.png"/></div>
                    <h5 class="tv-thongbao">Đặt vé thành công!
                    </h5>
                    <div class="tv-xacnhan-sub">Vui lòng kiểm tra email.</div>
                    <div class="tv-xacnhan-sub">Cám ơn bạn đã cho chúng tôi cơ hội được phục vụ!</div>
                    <div class="tv-xacnhan-hotline">Tư vấn miễn phí: <i>01233668804</i></div>
                </div>
            </div>
        </div>
    </div>
</div>

<s:include value="footer.jsp" />