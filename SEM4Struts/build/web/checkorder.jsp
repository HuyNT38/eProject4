<%-- 
    Document   : checkorder
    Created on : Aug 17, 2017, 11:49:15 PM
    Author     : letiep97
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<fmt:requestEncoding value="UTF-8" />
<%@taglib prefix="t" uri="/WEB-INF/tlds/customTag.tld" %>
<s:if test="%{boss==null}">
    <t:header price="0"
              title="Kiểm tra đơn hàng của bạn"/>  
</s:if>
<s:else>
    <t:header price="${boss.price}"
              title="Kiểm tra đơn hàng của bạn"/>
</s:else>
<s:if test="%{boss==null}">

    <s:form class="form-horizontal" method="get" action="checkorder" theme="css_xhtml">
        <div class="container tv-main-detail">

            <div class="tv-nhapthongtin">
                <div class="row">
                    <div class="col-sm-12 txtcenter">
                        <h1 class="tv-chonchuyen">
                            Tra cứu chuyến bay của bạn
                        </h1>
                        <p class="tv-desc">Quý khách nhập thông tin dưới đây để tra cứu thông tin hành trình</p>
                    </div>
                </div>

                <div class="tv-accinfo">
                    <h3>Xác nhận thông tin</h3>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6">
                            <label for="">Mã đặt chỗ</label>
                            <input name="code" type="text" placeholder="Mã đặt chỗ" autocomplete="off">
                        </div>
                        <div class="col-12 col-sm-12 col-md-6">
                            <label for="">Email hoặc Số điện thoại</label>
                            <input name="mailphone" type="text" placeholder="Email hoặc SĐT" autocomplete="off">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tv-barfooter">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <s:submit value="Tìm kiếm" cssClass="tv-chon"/>
                    </div>
                </div>
            </div>
        </div>
    </s:form>
</s:if>
<s:else>
    <div class="container tv-main-detail">
        <div class="container tv-xacnhan tv-checkorder">
            <div class="row tv-hbooking txtcenter">
                <div class="col-12">
                    <c:if test="${boss.status==0||boss.status==1}">
                    <h6>${ttchuyendi.ddName} đến ${ttchuyendi.ddName2}</h6>
                    <c:if test="${boss.status==0}">
                        <h4 class="tv-order-warning"> <img class="tv-icon-svg" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMjQiIHdpZHRoPSIyNCIgdmVyc2lvbj0iMS4xIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iPgogPGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCAtMTAyOC40KSI+CiAgPHBhdGggZD0ibTEyIDEwMzEuNGMtMC45ODUtMC4xLTEuNzE1IDAuNy0yLjAzMTMgMS42LTIuNjA1MyA1LjItNS4yMDg4IDEwLjQtNy44MTI1IDE1LjYtMC42MTc4IDEuMyAwLjU4MDIgMi45IDIgMi44aDcuODQzOCA3Ljg0NGMxLjQyIDAuMSAyLjYxOC0xLjUgMi0yLjgtMi42MDQtNS4yLTUuMjA3LTEwLjQtNy44MTMtMTUuNi0wLjMxNi0wLjktMS4wNDYtMS43LTIuMDMxLTEuNnoiIGZpbGw9IiNmMzljMTIiLz4KICA8cGF0aCBkPSJtMTIgMmMtMC45ODUtMC4wMzcyLTEuNzE1IDAuNzY4Mi0yLjAzMTIgMS42MjUtMi42MDU0IDUuMjEwNi01LjIwODkgMTAuNDE4LTcuODEyNiAxNS42MjUtMC42MTc4IDEuMzA3IDAuNTgwMiAyLjkxOSAyIDIuNzUgMi42MTAxLTAuMDAzIDUuMjMzNy0wLjAwMSA3Ljg0MzggMCAyLjYxLTAuMDAxIDUuMjM0LTAuMDAzIDcuODQ0IDAgMS40MiAwLjE2OSAyLjYxOC0xLjQ0MyAyLTIuNzUtMi42MDQtNS4yMDctNS4yMDctMTAuNDE0LTcuODEzLTE1LjYyNS0wLjMxNi0wLjg1NjgtMS4wNDYtMS42NjIyLTIuMDMxLTEuNjI1eiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCAxMDI4LjQpIiBmaWxsPSIjZjFjNDBmIi8+CiAgPHBhdGggZD0ibTEyIDhjLTAuNTUyIDAtMSAwLjQ0NzctMSAxbDAuNSA3aDFsMC41LTdjMC0wLjU1MjMtMC40NDgtMS0xLTF6bTAgOWMtMC41NTIgMC0xIDAuNDQ4LTEgMXMwLjQ0OCAxIDEgMSAxLTAuNDQ4IDEtMS0wLjQ0OC0xLTEtMXoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAgMTAyOC40KSIgZmlsbD0iIzM0NDk1ZSIvPgogPC9nPgo8L3N2Zz4K"> Chưa thanh toán</h5>
                        </c:if>
                    <c:if test="${boss.status==1}">
                            <h4 class="tv-order-success"> <img class="tv-icon-svg" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMjQiIHdpZHRoPSIyNCIgdmVyc2lvbj0iMS4xIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iPgogPGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCAtMTAyOC40KSI+CiAgPHBhdGggZD0ibTIyIDEyYzAgNS41MjMtNC40NzcgMTAtMTAgMTAtNS41MjI4IDAtMTAtNC40NzctMTAtMTAgMC01LjUyMjggNC40NzcyLTEwIDEwLTEwIDUuNTIzIDAgMTAgNC40NzcyIDEwIDEweiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCAxMDI5LjQpIiBmaWxsPSIjMjdhZTYwIi8+CiAgPHBhdGggZD0ibTIyIDEyYzAgNS41MjMtNC40NzcgMTAtMTAgMTAtNS41MjI4IDAtMTAtNC40NzctMTAtMTAgMC01LjUyMjggNC40NzcyLTEwIDEwLTEwIDUuNTIzIDAgMTAgNC40NzcyIDEwIDEweiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCAxMDI4LjQpIiBmaWxsPSIjMmVjYzcxIi8+CiAgPHBhdGggZD0ibTE2IDEwMzcuNC02IDYtMi41LTIuNS0yLjEyNSAyLjEgMi41IDIuNSAyIDIgMC4xMjUgMC4xIDguMTI1LTguMS0yLjEyNS0yLjF6IiBmaWxsPSIjMjdhZTYwIi8+CiAgPHBhdGggZD0ibTE2IDEwMzYuNC02IDYtMi41LTIuNS0yLjEyNSAyLjEgMi41IDIuNSAyIDIgMC4xMjUgMC4xIDguMTI1LTguMS0yLjEyNS0yLjF6IiBmaWxsPSIjZWNmMGYxIi8+CiA8L2c+Cjwvc3ZnPgo="> Đã thanh toán</h5>
                    </c:if>
                    </c:if>
                    <c:if test="${boss.status==3}">
                        <h6>Vé của ${boss.fname} ${boss.lname}</h6>
                                <h4 class="tv-order-error"> <img class="tv-icon-svg" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+CjxzdmcgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMjQiIHdpZHRoPSIyNCIgdmVyc2lvbj0iMS4xIiB4bWxuczpjYz0iaHR0cDovL2NyZWF0aXZlY29tbW9ucy5vcmcvbnMjIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iPgogPGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCAtMTAyOC40KSI+CiAgPHBhdGggZD0ibTIyIDEyYzAgNS41MjMtNC40NzcgMTAtMTAgMTAtNS41MjI4IDAtMTAtNC40NzctMTAtMTAgMC01LjUyMjggNC40NzcyLTEwIDEwLTEwIDUuNTIzIDAgMTAgNC40NzcyIDEwIDEweiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCAxMDI5LjQpIiBmaWxsPSIjYzAzOTJiIi8+CiAgPHBhdGggZD0ibTIyIDEyYzAgNS41MjMtNC40NzcgMTAtMTAgMTAtNS41MjI4IDAtMTAtNC40NzctMTAtMTAgMC01LjUyMjggNC40NzcyLTEwIDEwLTEwIDUuNTIzIDAgMTAgNC40NzcyIDEwIDEweiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCAxMDI4LjQpIiBmaWxsPSIjZTc0YzNjIi8+CiAgPHJlY3QgaGVpZ2h0PSI0IiB3aWR0aD0iMTIiIHk9IjEwMzkuNCIgeD0iNiIgZmlsbD0iI2MwMzkyYiIvPgogIDxyZWN0IGhlaWdodD0iMyIgd2lkdGg9IjEyIiB5PSIxMDM5LjQiIHg9IjYiIGZpbGw9IiNlY2YwZjEiLz4KIDwvZz4KPC9zdmc+Cg=="> Đã bị huỷ</h5>
                    </c:if>

                                <span></span>
                                </div>
                                </div>
                                <c:if test="${boss.status!=3}">
                                    <div class="row subbooking">
                                        Khởi hành lúc ${ttchuyendi.timeDi} ${ttchuyendi.dateFull}
                                    </div>
                                    <div class="row tv-booking">
                                        <div class="container tv-checkorder-container">
                                            <div class="row">
                                                <div class="col-12 col-md-6 tv-booking-child">
                                                    <div class="tv-iconbooking">
                                                        <i class="icon--lg icon-fare-primary"></i>
                                                    </div>
                                                    <div class="tv-bookgrow">
                                                        <h5>Chuyến bay ${ttchuyendi.name}</h5>
                                                        <div class="tv-bookingitem">
                                                            <span>${ttchuyendi.nl} x Người lớn</span>
                                                            <span>
                                                                <fmt:formatNumber type="number" value="${ttchuyendi.nl*ttchuyendi.price}" /> <i>VND</i>
                                                            </span>
                                                        </div>
                                                        <c:if test="${ttchuyendi.te>=1}">
                                                            <div class="tv-bookingitem">
                                                                <span>${ttchuyendi.te} x Trẻ em</span>
                                                                <span>
                                                                    <fmt:formatNumber type="number" value="${ttchuyendi.te*ttchuyendi.price}" /> <i>VND</i>
                                                                </span>
                                                            </div>
                                                        </c:if>
                                                        <c:if test="${ttchuyendi.eb>=1}">
                                                            <div class="tv-bookingitem">
                                                                <span>${ttchuyendi.eb} x Em bé</span>
                                                                <span><fmt:formatNumber type="number" value="${ttchuyendi.eb*ttchuyendi.priceeb}" /> <i>VND</i></span>
                                                            </div>
                                                        </c:if>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6 tv-booking-child">
                                                    <div class="tv-iconbooking">
                                                        <i class="icon--lg icon-fare-people"></i>
                                                    </div>
                                                    <div class="tv-bookgrow">
                                                        <h5>Khách hàng</h5>
                                                        <div class="tv-checkorder-people">
                                                            <div class="customer-left">                        
                                                                <p>Họ và Tên:</p>                      
                                                                <p>Điện thoại:</p>                        
                                                                <p>Email:</p>
                                                            </div>
                                                            <div class="customer-right">
                                                                <p>${boss.fname} ${boss.lname}</p>
                                                                <p>${boss.phone}</p>
                                                                <p style="word-wrap:break-word;">${boss.email}</p>
                                                            </div>
                                                            <div class="tv-checkorder-barcode text-center">
                                                                <img src="http://tools.sinhvienit.net/barcode/image.php?code=code128&o=1&dpi=172&t=30&r=1&rot=0&text=${boss.id}&f1=Arial.ttf&f2=8&a1=&a2=" alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row tv-booking">
                                        <div class="tv-iconbooking">
                                            <i class="icon--lg icon-seats-primary"></i>
                                        </div>
                                        <div class="tv-bookgrow">
                                            <h5>Chỗ ngồi</h5>
                                            <c:set var="count" value="0" scope="page" />
                                            <c:forEach var="a" items="${listkh}" varStatus="status" >  
                                                <c:if test="${a.type==1||a.type==2}">
                                                    <div class="tv-bookingitem">
                                                        <span>${a.fname} ${a.lname} - ${a.idG} 
                                                            <i>
                                                            <c:if test="${a.typeG==1}">
                                                                (ghế phổ thông)
                                                            </c:if>
                                                            <c:if test="${a.typeG==3}">
                                                                (ghế thương gia)
                                                            </c:if>
                                                            <c:if test="${a.typeG==2}">
                                                                (ghế hạng nhất)
                                                            </c:if>
                                                            </i>
                                                        </span>
                                                        <span><fmt:formatNumber type="number" value="${a.priceG}" /> <i>VND</i></span>
                                                    </div>
                                                    <c:set var="count" value="${count+a.priceG}" scope="page"/>
                                                </c:if>
                                            </c:forEach>

                                        </div>
                                    </div>
                                    <div class="row tv-bookings">
                                        <span>Tổng tiền đặt chỗ</span>
                                        <span>
                                            <fmt:formatNumber type="number" value="${boss.price}" />
                                            <i>VND</i>
                                        </span>
                                    </div>
                                </c:if>
                                </div>
                                </div>
                            </s:else>


                            <s:include value="footer.jsp" />