package org.apache.jsp.admin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class listBoss_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/bootstrap.min.css\">\n");
      out.write("<script type=\"text/javascript\" src=\"http://code.jquery.com/jquery-1.10.2.js\"></script>\n");
      out.write("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css\" integrity=\"sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M\" crossorigin=\"anonymous\">\n");
      out.write("<script type=\"text/javascript\">\n");
      out.write("// function for fetching user information from database\n");
      out.write("\tfunction report() {\t\t\n");
      out.write("\t\t$.ajax({\t\t\t\n");
      out.write("\t\t\ttype:\"GET\",\n");
      out.write("\t\t\turl:\"listBoss.html\",\n");
      out.write("\t\t\tsuccess: function(result){\n");
      out.write("\t\t\t\tvar tblData=\"\";\n");
      out.write("\t\t\t\t$.each(result.listBoss, function() {\t\t\t\t\t\n");
      out.write("\t\t\t\t\ttblData += \"<tr><td>\" + this._lname + \"</td>\" + \n");
      out.write("\t\t\t\t\t\"<td>\" + this._fname + \"</td>\" + \n");
      out.write("\t\t\t\t\t\"<td>\" + this._email + \"</td>\" + \n");
      out.write("\t\t\t\t\t\"<td>\" + this._phone + \"</td>\" +\n");
      out.write("\t\t\t\t\t\"<td>\"+\n");
      out.write("\t\t\t\t\t\"<button onclick='fetchOldRecord(this);' class='btn btn-sm btn-info' data-toggle='modal' data-target='#updateModal'>Update</button>\"+\n");
      out.write("\t\t\t\t\t\"<button onclick='deleteUser(this);' class='btn btn-sm btn-danger'>Delete</button>\"+\n");
      out.write("\t\t\t\t\t\"</td></tr>\" ;\n");
      out.write("\t\t\t\t});\n");
      out.write("\t\t\t\t$(\"#tbody\").html(tblData);\n");
      out.write("\t\t\t},\n");
      out.write("\t\t\terror: function(result){\n");
      out.write("\t\t\t\talert(\"Some error occured.\");\n");
      out.write("\t\t\t}\n");
      out.write("\t\t});\n");
      out.write("\t}\n");
      out.write("\t\n");
      out.write("\t// function for fecthing old information into the form\n");
      out.write("\tfunction fetchOldRecord(that){\t\t\n");
      out.write("\t\t   $(\"#uname\").val($(that).parent().prev().prev().prev().prev().text());\n");
      out.write("\t\t   $(\"#uemail\").val($(that).parent().prev().prev().prev().text());\n");
      out.write("\t\t   $(\"#upass\").val(\"\");\n");
      out.write("\t\t   $(\"#udeg\").val($(that).parent().prev().text());\n");
      out.write("\t\t   $(\"#hiddenuemail\").val($(that).parent().prev().prev().prev().text());\n");
      out.write("       \t}\n");
      out.write("\t\n");
      out.write("\t// function for updating new information into database\n");
      out.write("\t\n");
      out.write("\t\n");
      out.write("\t\n");
      out.write("</script>\n");
      out.write("</head>\n");
      out.write("<body onload=\"report();\">\n");
      out.write("\t<nav class=\"navbar navbar-default\">\n");
      out.write("\t\t<div class=\"container\">\n");
      out.write("\t\t\t<div class=\"navbar-header\">\n");
      out.write("\t\t\t\t<a class=\"navbar-brand\" href=\"/\">CRUD</a>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t<ul class=\"nav navbar-nav\">\n");
      out.write("\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t<a href=\"index.jsp\">Register</a>\n");
      out.write("\t\t\t\t</li>\n");
      out.write("\t\t\t\t<li>\n");
      out.write("\t\t\t\t\t<a href=\"report.jsp\">Report</a>\n");
      out.write("\t\t\t\t</li>\n");
      out.write("\t\t\t</ul>\n");
      out.write("\t\t</div>\n");
      out.write("\t</nav>\n");
      out.write("\t<div class=\"container\">\n");
      out.write("\t\t<table class=\"table table-bordered\">\n");
      out.write("\t\t\t<thead>\n");
      out.write("\t\t\t\t<tr class=\"bg-info\">\n");
      out.write("\t\t\t\t\t<th>Name</th>\n");
      out.write("\t\t\t\t\t<th>Email</th>\n");
      out.write("\t\t\t\t\t<th>Password</th>\n");
      out.write("\t\t\t\t\t<th>Designation</th>\n");
      out.write("\t\t\t\t\t<th>Action</th>\n");
      out.write("\t\t\t\t</tr>\n");
      out.write("\t\t\t</thead>\n");
      out.write("\t\t\t<tbody id=\"tbody\">\n");
      out.write("\t\t\t</tbody>\n");
      out.write("\t\t</table>\n");
      out.write("\t</div>\n");
      out.write("<div class=\"container\" id=\"updateBlock\">\n");
      out.write("\t\t<div class=\"modal fade\" id=\"updateModal\" role=\"dialog\">\n");
      out.write("\t\t\t<div class=\"modal-dialog\">\n");
      out.write("\t\t\t\t<div class=\"modal-content\">\n");
      out.write("\t\t\t\t\t<div class=\"modal-header\">\n");
      out.write("\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">×</button>\n");
      out.write("\t\t\t\t\t\t<h4 class=\"modal-title\">Update New Information</h4>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t<div class=\"modal-body\">\n");
      out.write("\t\t\t\t\t\t<div class=\"row\">\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-xs-6 col-sm-6 col-md-6\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"uname\" id=\"uname\" class=\"form-control input-sm\" placeholder=\"Full Name\">\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"col-xs-6 col-sm-6 col-md-6\">\n");
      out.write("\t\t\t\t\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t\t\t\t\t<input type=\"text\" name=\"udeg\" id=\"udeg\" class=\"form-control input-sm\" placeholder=\"Designation\">\n");
      out.write("\t\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t\t\t<input type=\"text\" name=\"uemail\" id=\"uemail\" class=\"form-control input-sm\" placeholder=\"Email\">\n");
      out.write("\t\t\t\t\t\t\t<input type=\"hidden\" name=\"hiddenuemail\" id=\"hiddenuemail\">\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<div class=\"form-group\">\n");
      out.write("\t\t\t\t\t\t\t<input type=\"password\" name=\"upass\" id=\"upass\" class=\"form-control input-sm\" placeholder=\"Password\">\n");
      out.write("\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t<button onclick=\"updateNewRecord();\" class=\"btn btn-info btn-block\">Update</button>\n");
      out.write("\t\t\t\t\t\t<div id=\"resp\" class=\"text-center\" style=\"margin-top: 13px;\"></div>\n");
      out.write("\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t</div>\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t</div>\n");
      out.write("\t</div>\n");
      out.write("</body>\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
