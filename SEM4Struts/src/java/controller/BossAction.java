/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionSupport;
import entity.Boss;
import java.util.ArrayList;
import model.BossModel;

/**
 *
 * @author letiep97
 */
public class BossAction extends ActionSupport {

    private String id, type;
    ArrayList<Boss> listBoss, data = new ArrayList<>();

    public String execute() throws Exception {
        listBoss = BossModel.listBoss();
        data = BossModel.listBoss();
        return SUCCESS;
    }

    public String confirm() {
        BossModel.confirmBoss(type, id);
        return SUCCESS;
    }

    public ArrayList<Boss> getData() {
        return data;
    }

    public void setData(ArrayList<Boss> data) {
        this.data = data;
    }

    public ArrayList<Boss> getListBoss() {
        return listBoss;
    }

    public void setListBoss(ArrayList<Boss> listBoss) {
        this.listBoss = listBoss;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
