/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionSupport;
import entity.Boss;
import entity.KhachHangCustom;
import entity.ThongTinChuyenDi;
import java.util.ArrayList;
import java.util.Map;
import model.ConfirmModel;
import model.SentMailModel;
import model.SentMailv2Model;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author letiep97
 */
public class ConfirmActionSupport extends ActionSupport implements SessionAware {

    private Map session;
    private String idBoss;

    public ConfirmActionSupport() {
    }

    public String execute() {
        if (session.get("ttchuyendi") == null || (session.get("ttchuyendi").equals(""))) {
            return ERROR;
        }
        ThongTinChuyenDi ttchuyendi = (ThongTinChuyenDi) session.get("ttchuyendi");
        Boss boss = (Boss) session.get("boss");
        ArrayList<KhachHangCustom> khachhang = (ArrayList<KhachHangCustom>) session.get("listkh");

        idBoss = ConfirmModel.Comfirm(ttchuyendi, boss, khachhang);
        SentMailv2Model.sentmail(ttchuyendi, boss, khachhang, idBoss);

        session.remove("ttchuyendi");
        session.remove("boss");
        session.remove("listkh");
        return SUCCESS;
    }

    @Override
    public void setSession(Map<String, Object> map) {
        this.session = map;
    }

    public String getIdBoss() {
        return idBoss;
    }

    public void setIdBoss(String idBoss) {
        this.idBoss = idBoss;
    }

}
