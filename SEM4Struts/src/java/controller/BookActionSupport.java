/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import entity.KhachHangCustom;
import entity.Seat;
import entity.ThongTinChuyenDi;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.DatChoModel;
import model.GheModel;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author letiep97
 */
public class BookActionSupport extends ActionSupport implements SessionAware {

    ArrayList<Seat> listSeat = new ArrayList<>();
    ArrayList<Seat> listSeatAuto = new ArrayList<>();
    ArrayList<String> listSeatChose = new ArrayList<>();
    private int priceSeat = 0, priceSum = 0;
    private Map session;

    public BookActionSupport() {
    }

    public String execute() throws Exception {
        session = ActionContext.getContext().getSession();
        ThongTinChuyenDi ttchuyendi = (ThongTinChuyenDi) session.get("ttchuyendi");
        listSeat = GheModel.listSeat();
        listSeatAuto = DatChoModel.autoChoseSeat(ttchuyendi.getId(), ttchuyendi.getNl() + ttchuyendi.getTe());
        listSeatChose = DatChoModel.seatCheck(ttchuyendi.getId());
        for (int i = 0; i < listSeatAuto.size(); i++) {
            priceSeat += listSeatAuto.get(i).getPrice();
        }
        return SUCCESS;
    }

    public String xacnhan() {
        if (session.get("ttchuyendi") == null || (session.get("ttchuyendi").equals(""))) {
            return ERROR;
        }
        HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
        ThongTinChuyenDi ttchuyendi = (ThongTinChuyenDi) session.get("ttchuyendi");
        ArrayList<KhachHangCustom> khachhang = (ArrayList<KhachHangCustom>) session.get("listkh");

        for (int i = 0; i < (ttchuyendi.getNl() + ttchuyendi.getTe()); i++) {
            khachhang.get(i).setIdG(request.getParameter("idG" + (i + 1)));
            if (khachhang.get(i).getIdG().equals("") || khachhang.get(i).getIdG() == null) {
                return NONE;
            }
            khachhang.get(i).setPriceG(GheModel.getPrice(request.getParameter("idG" + (i + 1))));
            priceSum += khachhang.get(i).getPriceG();
            khachhang.get(i).setTypeG(GheModel.getType(request.getParameter("idG" + (i + 1))));
        }

        priceSum += (ttchuyendi.getNl() + ttchuyendi.getTe()) * ttchuyendi.getPrice()
                + ttchuyendi.getEb() * ttchuyendi.getPriceeb();
        session.put("listkh", khachhang);
        return SUCCESS;
    }

    public int getPriceSeat() {
        return priceSeat;
    }

    public void setPriceSeat(int priceSeat) {
        this.priceSeat = priceSeat;
    }

    public ArrayList<Seat> getListSeat() {
        return listSeat;
    }

    public void setListSeat(ArrayList<Seat> listSeat) {
        this.listSeat = listSeat;
    }

    public ArrayList<Seat> getListSeatAuto() {
        return listSeatAuto;
    }

    public void setListSeatAuto(ArrayList<Seat> listSeatAuto) {
        this.listSeatAuto = listSeatAuto;
    }

    public ArrayList<String> getListSeatChose() {
        return listSeatChose;
    }

    public void setListSeatChose(ArrayList<String> listSeatChose) {
        this.listSeatChose = listSeatChose;
    }

    public int getPriceSum() {
        return priceSum;
    }

    public void setPriceSum(int priceSum) {
        this.priceSum = priceSum;
    }

    @Override
    public void setSession(Map<String, Object> map) {
        this.session = map;
    }

}
