/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import entity.Boss;
import entity.KhachHangCustom;
import entity.ThongTinChuyenDi;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import model.BossModel;
import model.SearchModel;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author letiep97
 */
public class CheckOrderActionSupport extends ActionSupport {
    private Boss boss;
    private ArrayList<KhachHangCustom> listkh;
    private ThongTinChuyenDi ttchuyendi;

    public CheckOrderActionSupport() {
    }

    public String execute() throws Exception {
        try {
            HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
            int code = Integer.valueOf(request.getParameter("code"));
            String mailphone = request.getParameter("mailphone");
            boss = BossModel.searchBoss(code, mailphone);
            if (boss != null) {
                if (boss.getStatus() ==1 || boss.getStatus() ==0) {
                    listkh = SearchModel.listKhachHang(code);
                    ttchuyendi = SearchModel.ttChuyenDi(code);
                }
            }
            else{
                return ERROR;
            }
        } catch (Exception e) {
            return ERROR;
        }
        return SUCCESS;
    }

    public Boss getBoss() {
        return boss;
    }

    public void setBoss(Boss boss) {
        this.boss = boss;
    }

    public ArrayList<KhachHangCustom> getListkh() {
        return listkh;
    }

    public void setListkh(ArrayList<KhachHangCustom> listkh) {
        this.listkh = listkh;
    }

    public ThongTinChuyenDi getTtchuyendi() {
        return ttchuyendi;
    }

    public void setTtchuyendi(ThongTinChuyenDi ttchuyendi) {
        this.ttchuyendi = ttchuyendi;
    }

}
