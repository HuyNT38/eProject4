/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import entity.ChuyenBayCustom;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.ChuyenBayModel;
import model.DiaDiemModel;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author letiep97
 */
public class SearchFlightAction extends ActionSupport {
        private List<ChuyenBayCustom> list = new ArrayList<>();
        private ChuyenBayModel db = new ChuyenBayModel();
        private DiaDiemModel dbDD = new DiaDiemModel();
        private String btnNext, btnPrev, dateFull,ddName,ddName2;
    
    public String qfilight() {
        HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
        String d1 = request.getParameter("d1");
        String d2 = request.getParameter("d2");
        String date = request.getParameter("date");
        String type = request.getParameter("type");
        String rdate = request.getParameter("rdate");
        String nl = request.getParameter("nl");
        String te = request.getParameter("te");
        String eb = request.getParameter("eb");
        
        if (d1.equals(d2)) {
            return ERROR;
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate datelo = LocalDate.parse(date, formatter);
        
        Locale vn = new Locale("vi", "VN");
        DayOfWeek w = datelo.getDayOfWeek();
        Month thang = datelo.getMonth();
        int ngaythang = datelo.getDayOfMonth();
        String ngay = w.getDisplayName(TextStyle.FULL, vn);
        String thangV = thang.getDisplayName(TextStyle.NARROW, vn);
        
        ddName = dbDD.getNameDDbyId(d1);
        ddName2 = dbDD.getNameDDbyId(d2);
        dateFull = ngay + ", ngày " + ngaythang + " tháng " + thangV + " năm " + datelo.getYear();
        
        btnNext = datelo.plusDays(1).format(formatter);
        btnPrev = datelo.minusDays(1).format(formatter);
        
        
        
        list = db.findAir(d1, d2, date, nl, te);
        return SUCCESS;
    }

    public List<ChuyenBayCustom> getList() {
        return list;
    }

    public void setList(List<ChuyenBayCustom> list) {
        this.list = list;
    }

    public String getBtnNext() {
        return btnNext;
    }

    public void setBtnNext(String btnNext) {
        this.btnNext = btnNext;
    }

    public String getBtnPrev() {
        return btnPrev;
    }

    public void setBtnPrev(String btnPrev) {
        this.btnPrev = btnPrev;
    }

    public String getDateFull() {
        return dateFull;
    }

    public void setDateFull(String dateFull) {
        this.dateFull = dateFull;
    }

    public String getDdName() {
        return ddName;
    }

    public void setDdName(String ddName) {
        this.ddName = ddName;
    }

    public String getDdName2() {
        return ddName2;
    }

    public void setDdName2(String ddName2) {
        this.ddName2 = ddName2;
    }


}
