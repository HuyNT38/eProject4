/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import entity.Boss;
import entity.EmBe;
import entity.KhachHangCustom;
import entity.ThongTinChuyenDi;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import model.ChuyenBayModel;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;

/**
 *
 * @author letiep97
 */
public class InsertActionSupport extends ActionSupport implements SessionAware {
    private final int priceeb = EmBe.priceeb;
    private Map session;

    public InsertActionSupport() {
    }

    public String execute() throws Exception {
        HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
        int id = Integer.valueOf(request.getParameter("id"));
        int nl = Integer.valueOf(request.getParameter("nl"));
        int te = Integer.valueOf(request.getParameter("te"));
        int eb = Integer.valueOf(request.getParameter("eb"));
        
        ThongTinChuyenDi ttchuyendi = new ThongTinChuyenDi();
        ttchuyendi.setId(id);
        ttchuyendi.setNl(nl);
        ttchuyendi.setEb(eb);
        ttchuyendi.setTe(te);
        ttchuyendi.setPrice(ChuyenBayModel.getPrice(id));
        ttchuyendi.setPriceeb(priceeb);
        session.put("ttchuyendi", ttchuyendi);

        return SUCCESS;
    }

    public String putSession() {
        HttpServletRequest request = (HttpServletRequest) ActionContext.getContext().get(ServletActionContext.HTTP_REQUEST);
        int id = Integer.valueOf(request.getParameter("id"));
        int nl = Integer.valueOf(request.getParameter("nl"));
        int te = Integer.valueOf(request.getParameter("te"));
        int eb = Integer.valueOf(request.getParameter("eb"));
        
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        
        Boss boss = new Boss();
        boss.setFname(fname);
        boss.setLname(lname);
        boss.setEmail(email);
        boss.setPhone(phone);
       // Boss boss = new Boss(0, fname, lname, email, phone, "");

        session = ActionContext.getContext().getSession();
        ArrayList<KhachHangCustom> list = new ArrayList<>();

        for (int i = 1; i <= nl; i++) {
            KhachHangCustom emp = new KhachHangCustom();
            emp.setId(Integer.valueOf(request.getParameter("nlid" + i)));
            emp.setFname(request.getParameter("nlfname" + i));
            emp.setLname(request.getParameter("nllname" + i));
            emp.setGender(Integer.valueOf(request.getParameter("nlgender" + i)));
            emp.setType(1);
            list.add(emp);
        }
        for (int i = 1; i <= te; i++) {
            KhachHangCustom emp = new KhachHangCustom();
            emp.setId(Integer.valueOf(request.getParameter("teid" + i)));
            emp.setFname(request.getParameter("tefname" + i));
            emp.setLname(request.getParameter("telname" + i));
            emp.setGender(Integer.valueOf(request.getParameter("tegender" + i)));
            emp.setType(2);
            list.add(emp);
        }
        for (int i = 1; i <= eb; i++) {
            KhachHangCustom emp = new KhachHangCustom();
            emp.setId(Integer.valueOf(request.getParameter("ebid" + i)));
            emp.setFname(request.getParameter("ebfname" + i));
            emp.setLname(request.getParameter("eblname" + i));
            emp.setGender(Integer.valueOf(request.getParameter("ebgender" + i)));
            emp.setWith(Integer.valueOf(request.getParameter("ebwith" + i)));
            emp.setDate(request.getParameter("ebyear" + i) + "-"
                    + request.getParameter("ebmonth" + i) + "-" + request.getParameter("ebday" + i));
            emp.setType(3);
            list.add(emp);
        }

        session.put("listkh", list);
        session.put("boss", boss);
        
        ThongTinChuyenDi ttchuyendi = new ThongTinChuyenDi();
        ttchuyendi.setId(id);
        ttchuyendi.setNl(nl);
        ttchuyendi.setEb(eb);
        ttchuyendi.setTe(te);
        ttchuyendi.setName(ChuyenBayModel.getName(id));
        ttchuyendi.setPrice(ChuyenBayModel.getPrice(id));
        ttchuyendi.setPriceeb(priceeb);
        ttchuyendi.setDdName(ChuyenBayModel.getNameDDi(id));
        ttchuyendi.setDdName2(ChuyenBayModel.getNameDDd(id));
        ttchuyendi.setDateFull(ChuyenBayModel.getDateFull(id));
        ttchuyendi.setTimeBay(ChuyenBayModel.getTimeBaybyID(id));
        ttchuyendi.setDd(ChuyenBayModel.getDDi(id));
        ttchuyendi.setDd2(ChuyenBayModel.getDDd(id));
        ttchuyendi.setTimeDi(ChuyenBayModel.getTimeDi(id));
        ttchuyendi.setTimeDd(ChuyenBayModel.getTimeDd(id));
        session.put("ttchuyendi", ttchuyendi);

//        ArrayList<KhachHang> dt = (ArrayList<KhachHang>) session.get("listkh");
//        for (int i = 0; i < dt.size(); i++) {
//            System.out.println("" + dt.get(i).getFname());
//        }
        return SUCCESS;
    }

    public String insertSession() {

        ArrayList<KhachHangCustom> dt = (ArrayList<KhachHangCustom>) session.get("listkh");

        return SUCCESS;
    }

    @Override
    public void setSession(Map<String, Object> map) {
        this.session = map;
    }

}
