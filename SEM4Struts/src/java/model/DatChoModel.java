/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.mysql.jdbc.Connection;
import entity.Seat;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author letiep97
 */
public class DatChoModel {

    public static ArrayList<Seat> listSeatOld(int id) {
        ArrayList<Seat> list = new ArrayList();
        try {
            String sql = "SELECT * FROM tblGhe";
            String sql2 = "SELECT * FROM tblVe WHERE _idCB=" + id;
            Connection conn = ConnectionFactory.getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                String _id = rs.getString(1);
                int type = 1;
                int price = rs.getInt(3);
                int status = 0;

                Seat temp = new Seat(_id, type, price, status);
                list.add(temp);
            }
            rs = st.executeQuery(sql2);

            while (rs.next()) {
                String _idG = rs.getString(3);
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getId().equals(_idG)) {
                        list.get(i).setStatus(1);
                    }
                }
            }

            st.close();
            rs.close();
            conn.close();

        } catch (SQLException ex) {
            Logger.getLogger(DatChoModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }



    public static ArrayList<String> seatCheck(int id) {
        ArrayList<String> list = new ArrayList<>();
        String sql = "SELECT * FROM tblVe WHERE _idCB=" + id;
        Connection conn = ConnectionFactory.getConnection();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            rs = st.executeQuery(sql);

            while (rs.next()) {
                String _idG = rs.getString(3);
                list.add(_idG);
            }
            st.close();
            rs.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(DatChoModel.class.getName()).log(Level.SEVERE, null, ex);
        }

        return list;
    }

    public static ArrayList<Seat> autoChoseSeat(int id, int s) {
        ArrayList<Seat> list = new ArrayList<>();
        ArrayList<Seat> listSeat = GheModel.listSeat();
        ArrayList<String> seatCheck = seatCheck(id);
        for (int i = 0; i < listSeat.size(); i++) {
            for (int j = 0; j < seatCheck.size(); j++) {
                if (listSeat.get(i).getId().equals(seatCheck.get(j))) {
                    listSeat.get(i).setStatus(1);
                }
            }
        }/// Đánh dấu những ghễ đã chọn =1

        for (int i = 0; i < listSeat.size(); i++) {
            System.out.println(listSeat.get(i).getId() + "---" + listSeat.get(i).getStatus());
        }/// In ra tình trạng ghế của chuyến bay

        int i = 0;//// Vị trí của ghế
        for (; i < listSeat.size(); ++i) {
            int son = 0;
            if (listSeat.get(i).getStatus() == 0&& listSeat.get(i).getType()==1) {
                /// Chỉ lập những ghế đã chọn và loại thường 
                ++son;
                for (int j = 1; j < s; j++) {/// Những ghế tiếp theo
                    if (listSeat.get(i + j).getStatus() == 0) {/// Nếu trống +1
                        ++son;
                    }
                }
            }
            if (son == (s)) {//// sống lượng ghế trống liên tiếp theo đủ số người thì thoát
                break;
            }
        }
        for (int j = 0; j < s; j++) {
            list.add(listSeat.get(i));// thêm vào list vị trí ghế đầu tiên tìm được  cho đến khi ++ < s
            i++;
        }
        return list;
    }

    public static void main(String[] args) {
//        ArrayList<Seat> list = listSeat();
//        for (int i = 0; i < list.size(); i++) {
//            System.out.println("" + list.get(i).getId()
//                    + "|" + list.get(i).getPrice()
//                    + "|" + list.get(i).getStatus());
//        }

        ArrayList<Seat> list2 = autoChoseSeat(9, 2);
        for (int i = 0; i < list2.size(); i++) {
            System.out.println("" + list2.get(i).getId());
        }
    }
}
