/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.BarChuyenBay;
import entity.ChuyenBayCustom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author letiep97
 */
public class ChuyenBayModel {

    public ArrayList<ChuyenBayCustom> findAir(String d1, String d2, String dateR, String nl, String te) {
        ArrayList<ChuyenBayCustom> list = new ArrayList();
        try {
            SimpleDateFormat formatDate = new SimpleDateFormat("dd-mm-yyyy");
            SimpleDateFormat formatSQL = new SimpleDateFormat("yyyy-mm-dd");
            SimpleDateFormat formatFull = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
            SimpleDateFormat formatH = new SimpleDateFormat("HH:mm");

            Date date = formatDate.parse(dateR);
            dateR = formatSQL.format(date);

            String sql = "SELECT * FROM tblChuyenBay WHERE _idDDi='"
                    + d1 + "' AND _idDDd='" + d2 + "' AND dateDi LIKE '" + dateR + "%' ORDER BY dateDi ASC";
            Connection conn = ConnectionFactory.getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                int id = Integer.parseInt(rs.getString(1));
                String idDDi = rs.getString(2);
                String idDDd = rs.getString(3);
                String nameDDi = DiaDiemModel.getNameDDbyId(rs.getString(2));
                String nameDDd = DiaDiemModel.getNameDDbyId(rs.getString(3));
                String name = rs.getString(4);
                String timeDi = formatH.format(formatFull.parse(rs.getString(5)));
                String timeVe = formatH.format(formatFull.parse(rs.getString(6)));
                String dateDi = ChuyenBayModel.stringToDateFull(
                        formatFull.format(formatFull.parse(rs.getString(5))));
                String dateVe = ChuyenBayModel.stringToDateFull(
                        formatFull.format(formatFull.parse(rs.getString(6))));
                String timeBay = ChuyenBayModel.getTimeBay(formatFull.format(formatFull.parse(rs.getString(5))),
                        formatFull.format(formatFull.parse(rs.getString(6))));
                int price = Integer.parseInt(rs.getString(7));
                int type = ChuyenBayModel.setTypeAir(id, Integer.parseInt(nl) + Integer.parseInt(te));

                ChuyenBayCustom temp = new ChuyenBayCustom(id, nameDDi, nameDDd, idDDi, idDDd, name, dateDi, dateVe, timeDi, timeVe, timeBay, price, type);
                list.add(temp);
            }
            st.close();
            rs.close();
            conn.close();

            int smallest = Integer.MAX_VALUE;
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getPrice() < smallest && list.get(i).getType() == 2) {
                    smallest = list.get(i).getPrice();
                }
            }
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getPrice() == smallest && list.get(i).getType() == 2) {
                    list.get(i).setType(4);
                }
            }

        } catch (ParseException ex) {
            Logger.getLogger(ChuyenBayModel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ChuyenBayModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static ArrayList<BarChuyenBay> findDayOfWeekFix(String d1, String d2, String dateR, String nl, String te) throws ParseException, SQLException {
        ArrayList<BarChuyenBay> listDay = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate datelo = LocalDate.parse(dateR, formatter);
        Locale vn = new Locale("vi", "VN");
        DateFormat localFormat = DateFormat.getDateInstance(DateFormat.FULL, vn);
        Calendar lich = Calendar.getInstance();
        Date date = lich.getTime();
        Date date1 = dateFormat.parse(dateR);
        lich.setTime(date1);

        for (int i = -3; i <= 3; i++) {
            BarChuyenBay emp = new BarChuyenBay();
            lich.add(Calendar.DATE, i);
            String dateI = datelo.plusDays(i).format(formatter);
            LocalDate datelo2 = LocalDate.parse(dateI, formatter);
            DayOfWeek w = datelo2.getDayOfWeek();
            date = lich.getTime();

            emp.setDay("Ngày " + String.valueOf(datelo2.getDayOfMonth()));
            emp.setDayS(String.valueOf(datelo2.getDayOfMonth()));
            emp.setDate(w.getDisplayName(TextStyle.FULL, vn));
            emp.setDateS(w.getDisplayName(TextStyle.FULL, vn));
            emp.setDateFull(localFormat.format(date));
            emp.setUrl(dateI);

            ArrayList<ChuyenBayCustom> list = ChuyenBayModel.findPriceSmallAir(d1, d2, dateI, nl, te);
            for (int j = 0; j < list.size(); j++) {
                if (list.get(j).getType() == 4) {
                    emp.setPrice(list.get(j).getPrice());
                    break;
                }
            }
            listDay.add(emp);

            //// Setup lai lich ve ngay ban dau
            date1 = dateFormat.parse(dateR);
            lich.setTime(date1);
        }
        return listDay;
    }

    public static ArrayList<ChuyenBayCustom> findPriceSmallAir(String d1, String d2, String dateR, String nl, String te) throws SQLException, ParseException {
        ArrayList<ChuyenBayCustom> list = new ArrayList();
        SimpleDateFormat formatDate = new SimpleDateFormat("dd-mm-yyyy");
        SimpleDateFormat formatSQL = new SimpleDateFormat("yyyy-mm-dd");

        Date date = formatDate.parse(dateR);
        dateR = formatSQL.format(date);

        String sql = "SELECT * FROM tblChuyenBay WHERE _idDDi='"
                + d1 + "' AND _idDDd='" + d2 + "' AND dateDi LIKE '" + dateR + "%' ORDER BY dateDi ASC";
        Connection conn = ConnectionFactory.getConnection();
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            int id = Integer.parseInt(rs.getString(1));
            int price = Integer.parseInt(rs.getString(7));
            int type = ChuyenBayModel.setTypeAir(id, Integer.parseInt(nl) + Integer.parseInt(te));
            ChuyenBayCustom temp = new ChuyenBayCustom();
            temp.setId(id);
            temp.setPrice(price);
            temp.setType(type);
            list.add(temp);
        }
        st.close();
        rs.close();
        conn.close();
        int smallest = Integer.MAX_VALUE;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getPrice() < smallest && list.get(i).getType() == 2) {
                smallest = list.get(i).getPrice();
            }
        }
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getPrice() == smallest && list.get(i).getType() == 2) {
                list.get(i).setType(4);
            }
        }
        return list;
    }

    public static int setTypeAir(int id, int sum) throws SQLException {
        int sizeGhe = 0, sizeVe = 0;
        Connection conn = new ConnectionFactory().getConnection();
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM tblGhe");
        if (rs.next()) {
            sizeGhe = rs.getInt(1);
        }

        rs = st.executeQuery("SELECT COUNT(*) FROM tblVe WHERE _idCB=" + id);
        if (rs.next()) {
            sizeVe = rs.getInt(1);
        }
        st.close();
        rs.close();
        conn.close();
        if (sizeGhe - sizeVe == 0) {
            return 1;//// Het ve
        }
        if (sizeGhe - sizeVe >= sum) {
            return 2;//// Con ve
        } else {
            return 3;/// Khong du ghe
        }

    }

    public static String getTimeBay(String di, String den) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();

        Date date1, date2;
        try {
            date1 = dateFormat.parse(di);
            date2 = dateFormat.parse(den);
            c1.setTime(date1);
            c2.setTime(date2);
        } catch (ParseException ex) {
            Logger.getLogger(ChuyenBayModel.class.getName()).log(Level.SEVERE, null, ex);
        }

        long noDay = (c2.getTime().getTime() - c1.getTime().getTime());
        if (TimeUnit.MILLISECONDS.toHours(noDay) > 24) {
            return String.format("%2d ngày %2d giờ %02d phút", TimeUnit.MILLISECONDS.toDays(noDay),
                    TimeUnit.MILLISECONDS.toHours(noDay) % 24,
                    TimeUnit.MILLISECONDS.toMinutes(noDay) % 60);
        }

        if ((TimeUnit.MILLISECONDS.toMinutes(noDay) % 60) == 0) {
            return String.format("%2d giờ", TimeUnit.MILLISECONDS.toHours(noDay));
        }
        return String.format("%2d giờ %02d phút", TimeUnit.MILLISECONDS.toHours(noDay) % 24,
                TimeUnit.MILLISECONDS.toMinutes(noDay) % 60);
    }

    public static String getTimeBaybyID(int id)  {
        SimpleDateFormat formatFull = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        String sql = "SELECT dateDi,dateDen FROM tblChuyenBay WHERE _id=" + id;
        String ddi = null, ddd = null;
        try {
            Statement st = ConnectionFactory.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                ddi = rs.getString(1);
                ddd = rs.getString(2);
            }
            rs.close();
            st.close();
            ConnectionFactory.getConnection().close();
        } catch (SQLException ex) {
        }
        try {
            sql = getTimeBay(formatFull.format(formatFull.parse(ddi)), formatFull.format(formatFull.parse(ddd)));
        } catch (ParseException ex) {
            Logger.getLogger(ChuyenBayModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sql;
    }

    public static String stringToDateFull(String string) throws ParseException {
        SimpleDateFormat formatFull = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        Date date = formatFull.parse(string);
        Locale vn = new Locale("vi", "VN");
        DateFormat localFormat = DateFormat.getDateInstance(DateFormat.FULL, vn);
        return localFormat.format(date);
    }

    public static int getPrice(int id) {
        String sql = "SELECT price FROM tblChuyenBay WHERE _id=" + id;
        try {
            Statement st = ConnectionFactory.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                id = rs.getInt(1);
            }
            rs.close();
            st.close();
            ConnectionFactory.getConnection().close();
        } catch (SQLException ex) {
        }
        return id;
    }
    
    public static String getName(int id){
        String sql = "SELECT _name FROM tblChuyenBay WHERE _id=" + id;
        try {
            Statement st = ConnectionFactory.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                sql = rs.getString(1);
            }
            rs.close();
            st.close();
            ConnectionFactory.getConnection().close();
        } catch (SQLException ex) {
        }
        return sql;
    }

    public static String getNameDDi(int id) {
        return DiaDiemModel.getNameDDbyId(getDDi(id));
    }

    public static String getNameDDd(int id) {
        return DiaDiemModel.getNameDDbyId(getDDd(id));
    }

    public static String getDDi(int id) {
        String sql = "SELECT _idDDi FROM tblChuyenBay WHERE _id=?";
        String name = "";
        try {
            Connection conn = ConnectionFactory.getConnection();
            PreparedStatement prst = conn.prepareStatement(sql);
            prst.setInt(1, id);
            ResultSet rs = prst.executeQuery();
            while (rs.next()) {
                name = rs.getString(1);
            }
            prst.close();
            rs.close();
            conn.close();
        } catch (SQLException ex) {
        }
        return name;
    }

    public static String getDDd(int id) {
        String sql = "SELECT _idDDd FROM tblChuyenBay WHERE _id=?";
        String name = "";
        try {
            Connection conn = ConnectionFactory.getConnection();
            PreparedStatement prst = conn.prepareStatement(sql);
            prst.setInt(1, id);
            ResultSet rs = prst.executeQuery();
            while (rs.next()) {
                name = rs.getString(1);
            }
            prst.close();
            rs.close();
            conn.close();
        } catch (SQLException ex) {
        }
        return name;
    }

    public static String getTimeDi(int id) {
        SimpleDateFormat formatFull = new SimpleDateFormat("yyyy-mm-dd HH:mm");
        SimpleDateFormat formatH = new SimpleDateFormat("HH:mm");
        String sql = "SELECT dateDi FROM tblChuyenBay WHERE _id=?";
        String name = "";
        try {
            Connection conn = ConnectionFactory.getConnection();
            PreparedStatement prst = conn.prepareStatement(sql);
            prst.setInt(1, id);
            ResultSet rs = prst.executeQuery();
            while (rs.next()) {
                name = formatH.format(formatFull.parse(rs.getString(1)));
            }
            prst.close();
            rs.close();
            conn.close();
        } catch (SQLException ex) {
        } catch (ParseException ex) {
            Logger.getLogger(ChuyenBayModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return name;
    }

    public static String getTimeDd(int id) {
        SimpleDateFormat formatFull = new SimpleDateFormat("yyyy-mm-dd HH:mm");
        SimpleDateFormat formatH = new SimpleDateFormat("HH:mm");
        String sql = "SELECT dateDen FROM tblChuyenBay WHERE _id=?";
        String name = "";
        try {
            Connection conn = ConnectionFactory.getConnection();
            PreparedStatement prst = conn.prepareStatement(sql);
            prst.setInt(1, id);
            ResultSet rs = prst.executeQuery();
            while (rs.next()) {
                name = formatH.format(formatFull.parse(rs.getString(1)));
            }
            prst.close();
            rs.close();
            conn.close();
        } catch (SQLException ex) {
        } catch (ParseException ex) {
            Logger.getLogger(ChuyenBayModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return name;
    }

    public static String getDateFull(int id) {
        String sql = "SELECT dateDi FROM tblChuyenBay WHERE _id=?";
        String date = "";
        try {
            Connection conn = ConnectionFactory.getConnection();
            PreparedStatement prst = conn.prepareStatement(sql);
            prst.setInt(1, id);
            ResultSet rs = prst.executeQuery();
            while (rs.next()) {
                date = rs.getString(1);
            }
            prst.close();
            rs.close();
            conn.close();
        } catch (SQLException ex) {
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
        LocalDate datelo = LocalDate.parse(date, formatter);

        Locale vn = new Locale("vi", "VN");
        DayOfWeek w = datelo.getDayOfWeek();
        Month thang = datelo.getMonth();
        int ngaythang = datelo.getDayOfMonth();
        String ngay = w.getDisplayName(TextStyle.FULL, vn);
        String thangV = thang.getDisplayName(TextStyle.NARROW, vn);

        date = ngay + ", ngày " + ngaythang + " tháng " + thangV + " năm " + datelo.getYear();
        return date;
    }

    public static void main(String[] args) throws ParseException, SQLException {
        System.out.println("Time Bay: "+ getTimeBay("2017-08-31 23:00:00.0", "2017-09-02 10:00:00.0"));
        System.out.println("getTimeBaybyID:" +getTimeBaybyID(9));
        System.out.println("getTimeDi:" + getTimeDi(9));
        System.out.println("getTimeDd:" + getTimeDd(9));
        System.out.println("getDateFull:" + getDateFull(3));
        System.out.println("getNameDDi: " + getNameDDi(9));
        System.out.println("getNameDDd " + getNameDDd(9));
        System.out.println("Price: " + getPrice(9));
        System.out.println("Name:" + getName(9));
        System.out.println("" + ChuyenBayModel.setTypeAir(1, 7));

        System.out.println(DiaDiemModel.getNameDDbyId("SGN"));

        // Định dạng thời gian
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm");

        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();

        // Định nghĩa 2 mốc thời gian ban đầu
        Date date1 = dateFormat.parse("2017-07-29 09:00");
        Date date2 = dateFormat.parse("2017-07-30 11:30");

        c1.setTime(date1);
        c2.setTime(date2);

        long noDay = (c2.getTime().getTime() - c1.getTime().getTime());

        String time = String.format("%2d giờ %02d phút", TimeUnit.MILLISECONDS.toHours(noDay),
                TimeUnit.MILLISECONDS.toMinutes(noDay) % 60);
        System.out.println(time);

        ArrayList<ChuyenBayCustom> cbC = new ArrayList<>();
        ChuyenBayModel db = new ChuyenBayModel();
        cbC = db.findAir("HAN", "SGN", "27-07-2017", "7", "1");
        for (int i = 0; i < cbC.size(); i++) {
            System.out.println("" + cbC.get(i).getDateDen()
                    + cbC.get(i).getDateDi()
                    + cbC.get(i).getTimeBay() + "|" + cbC.get(i).getTimeDi() + "|" + cbC.get(i).getTimeDen()
                    + cbC.get(i).getNameDDd());
        }

        ArrayList<BarChuyenBay> cbC2 = new ArrayList<>();
        cbC2 = ChuyenBayModel.findDayOfWeekFix("HAN", "SGN", "27-07-2017", "7", "1");
        for (int i = 0; i < cbC2.size(); i++) {
            System.out.println("" + cbC2.get(i).getDateFull() + "|" + cbC2.get(i).getDay()
                    + "|" + cbC2.get(i).getDayS() + "|" + cbC2.get(i).getDate() + "|" + cbC2.get(i).getDateS() + "| url:"
                    + cbC2.get(i).getUrl() + "| price:" + cbC2.get(i).getPrice()
            );
        }

//       ArrayList<ChuyenBayCustom> list = ChuyenBayModel.findAir("HAN", "SGN", "27-07-2017", "1", "1");
//            for (int j = 0; j < list.size(); j++) {
//                
//                if (list.get(j).getType() == 4) {
//                    System.out.println("tien"+list.get(j).getPrice());
//                    break;
//                }
//            }
    }
}
