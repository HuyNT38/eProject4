/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Boss;
import entity.KhachHangCustom;
import entity.ThongTinChuyenDi;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author letiep97
 */
public class SentMailv2Model {

    public static boolean sentmail(ThongTinChuyenDi ttchuyendi, Boss boss, ArrayList<KhachHangCustom> khachhang, String idBoss) {

        String deadline = deadline(Integer.valueOf(idBoss));
        int price = ConfirmModel.Total(ttchuyendi, khachhang);
        String ghe = "";
        for (int i = 0; i < khachhang.size(); i++) {
            if (khachhang.get(i).getType() == 1 || khachhang.get(i).getType() == 2) {
                ghe+="<div class=\"table-child\">\n" +
"                                    <div class=\"row\">\n" +
"                                        <div class=\"name-child\">"+ khachhang.get(i).getFname() + " " + khachhang.get(i).getLname() +"</div>\n" +
"                                        <div class=\"seat-child\">Chỗ ngồi <b>"+ khachhang.get(i).getIdG() +"</b></div>\n" +
"                                    </div>\n" +
"                                </div>\n";

                if (khachhang.get(i).getType() == 1) {
                    for (int j = 0; j < khachhang.size(); j++) {
                        if (khachhang.get(i).getId() == khachhang.get(j).getWith() && khachhang.get(j).getType() == 3) {
                            ghe += "<div class=\"table-son\">\n" +
"                                    <div class=\"row\">\n" +
"                                        <div class=\"name-child\">- "+ khachhang.get(j).getFname() + " " + khachhang.get(j).getLname() +"</div>\n" +
"                                        <div class=\"seat-child\">Đi cùng người lớn</div>\n" +
"                                    </div>\n" +
"                                </div>\n";
                        }
                    }
                }
            }

        }


        String contentv2="<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
"<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
"\n" +
"<head>\n" +
"    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
"    <!--[if !mso]><!-->\n" +
"    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" />\n" +
"    <!--<![endif]-->\n" +
"    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />\n" +
"    <title></title>\n" +
"    <!--[if (gte mso 9)|(IE)]>\n" +
"    <style type=\"text/css\">\n" +
"        table {border-collapse: collapse;}\n" +
"    </style>\n" +
"    <![endif]-->\n" +
"    <style type=\"text/css\">.body{font-family:sans-serif;line-height:1.5;color:#434343;max-width:600px;background:#fff;margin:24px auto}.text-center{text-align:center}.barheader{background:#fff url('https://ci6.googleusercontent.com/proxy/WmjRqPRnHDYcBVYmVBo1S9xENtEfTWQgyB2vSiYWOC6gRD2EwUN8i3mAIIgamVOMEAuRplQVQG2ris0AlBTIhOvRtCIr43VGCOg7aPeJKs2P6wgzH2FnpWulhdJXRc7iuwB1ezNJXMbuMWoeWRA_VG7UJIXr_WS1a-qrDE6NOlyvVmwFkpHYd0b1JjWjrSg7H82N=s0-d-e1-ft#https://s3-ap-southeast-1.amazonaws.com/traveloka/imageResource/2017/05/29/1496040205080-2eb4762f5c929a3a703b29543029a318.png') repeat-x -10% 0;display:block;font-weight:normal;height:3px;width:100%}.content{margin:24px 24px}.header{}.header img{max-width:150px}.title{margin:20px 0 0;font-size:1.2em}.deadline{margin-top:20px;display:flex}.deadline-left{float:left;width:50%}.deadline-right{float:left;width:50%}.code-deadline{padding:10px 30px;margin:0 0 0 60px;background:#f6f6f6;border:1px solid #dadada;border-radius:5px;font-size:14px;margin-bottom:10px}.code-deadline b{color:#0d7fcc;line-height:normal;display:block;font-size:28px}.kc-img img{padding-right:15px}.money{background:#f6f6f6;border:1px solid #dadada}.kc-money{font-size:80%;padding:15px}.foot-m{display:flex;background-color:#eaeaea;padding:15px}.foot-m-l{width:70%;float:left;line-height:normal}.foot-m-r{width:30%;float:left}.kc-img{padding:0 15px}.foot-m-l-title{font-size:14px}.foot-m-l-price{font-size:24px;font-weight:bold}.foot-m-l-price span{font-size:50%}.foot-m-r a{background:#f96d01;color:#fff;text-align:center;border-radius:5px;text-decoration:none;padding:10px 20px;display:block}.table-tt{border:1px solid #c4c1c0}.head-table{background:#1ba0e2;color:#fff;padding:15px}.table-kh{float:left;width:30%}.row{display:flex}.table-tgdd{font-size:14px;padding:10px 15px}.fulldate-table{float:left;width:60%}.dd-tg{float:left;width:40%;text-align:right}.table-tgkh{float:left;text-align:right;width:70%}.table-dd1{float:left;font-size:14px;width:45%}.table-dd2{float:left;width:45%;font-size:14px;text-align:right}.table-ddf{width:10%}.table-child{padding:6px 15px;border-top:1px solid #434352;background:#f6f6f6;font-size:14px}.name-child{float:left;width:60%}.seat-child{float:left;width:40%;text-align:right}.table-son{padding:6px 15px;padding-left:40px; background:#ececec;font-size:12px}.footer{margin-top:40px;border-top:1px solid #dadada;font-size:14px;padding-top:20px}.f-logo img{max-height:35px}.f-logo{margin-top:15px}.theend{height:5px}@media (max-width:575px){.body{margin-bottom:0}.deadline{display:block}.deadline-left{width:100%}.deadline-right{width:100%}.code-deadline{margin:10px auto 20px;max-width:180px}.foot-m{display:flow-root}.foot-m-l{width:100%}.foot-m-r{width:100%;margin-top:10px}.head-table .row:nth-child(2){margin-top:10px}}@media (max-width:400px){.content{margin:24px 1px}.kc-img img{padding-right:5px}}</style>\n" +
"</head>\n" +
"\n" +
"<body>\n" +
"    <table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color:#e6eaed;font-size: 16px;    font-family: arial, sans-serif\">\n" +
"        <tbody>\n" +
"            <tr>\n" +
"                <td>\n" +
"                    <div class=\"body\">\n" +
"                        <div class=\"barheader\"></div>\n" +
"                        <div class=\"content\">\n" +
"                            <div class=\"header text-center\">\n" +
"                                <img src=\"https://ci6.googleusercontent.com/proxy/-b1wWvsj3EbK7PGbmnC8QTxd94enNDAiU1Kj-4lD8A-8XuV-MxG8CmGYflrZSs5s6ldi4Q=s0-d-e1-ft#http://i.imgur.com/7j3fTOJ.png\" class=\"logo\" alt=\"AptechAir\" />\n" +
"                            </div>\n" +
"                            <div class=\"dear\">\n" +
"                                Kính gửi <b>"+ boss.getFname() + " " + boss.getLname() + "</b>,\n" +
"                            </div>\n" +
"                            <div class=\"title\">Đặt chỗ của quý khách từ <b>"+ttchuyendi.getDdName()+"</b> tới <b>"+ ttchuyendi.getDdName2() +"</b> đang được giữ trong 24 giờ!</div>\n" +
"                            <div class=\"deadline\">\n" +
"                                <div class=\"deadline-left\">\n" +
"                                    <div class=\"time-deadline\">\n" +
"                                        Hoàn tất thanh toán của quý khách trước <b>"+deadline+"</b>, nếu không đặt chỗ của quý khách sẽ bị hủy.\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"deadline-right\">\n" +
"                                    <div class=\"code-deadline text-center\">\n" +
"                                        <span>Mã đặt chỗ</span>\n" +
"                                        <b>"+idBoss+"</b>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                            <div class=\"title\">Hoàn tất đặt chỗ của quý khách</div>\n" +
"                            <div class=\"money\">\n" +
"                                <div class=\"head-m\">\n" +
"                                    <div class=\"kc-money\">Chúng tôi chấp nhận các phương thức thanh toán sau:</div>\n" +
"                                    <div class=\"kc-img\">\n" +
"                                        <img src=\"https://ci3.googleusercontent.com/proxy/wpYZ1OUv-GhT7yf8v78mVlsa11WOVcYoQbmq9Oz2STj1Qw8vRLQ19ytGWeAFc-zoHkL1d2DkhU0_M7Z5x1_QbgZq9O26UwhJOSOb4ReP9YhNZaru5Hw8TWtc-kJ8z4Kypue4PhRHklF714OgkMoZIkt8jk_Wm28W=s0-d-e1-ft#http://www.cathaypacific.com/content/dam/cx/applications/ibe/amadeus/email/payment/credit-visa.png\" alt=\"pay\" />\n" +
"                                        <img src=\"https://ci3.googleusercontent.com/proxy/-0mNtl1DFf1aA8AI8Lj3abJWmY-5uyLy16CIj5zFi-PnY3Rr6Z7NyX1YiQq_hQDo8rjfF875SOsN7UMVNBo7_uX4Jj8Tv_9gUPKCBw6vcUTmxHRntPcS_kXxQPB-u4ZIiQD1XiGoSJxScA6YbIF-fWOfnahhLtw=s0-d-e1-ft#http://www.cathaypacific.com/content/dam/cx/applications/ibe/amadeus/email/payment/credit-jcb.png\" alt=\"pay\" />\n" +
"                                        <img src=\"https://ci3.googleusercontent.com/proxy/cEv1V1P4XKKVqJKZsAmBchqFNlEaf1OEtzkguxEn5eLsQ8l_UA-B_W6oEYd0ny6yn5hI40aQfu93MOXAtkRy1jP3VImIIjegrtEaEE9-K8AJTpPYkFmqsRJ1j_XrUJoTOK3HNUky2uKHygv9mCbvoqBNcts_nUzdXQ=s0-d-e1-ft#http://www.cathaypacific.com/content/dam/cx/applications/ibe/amadeus/email/payment/credit-union.png\" alt=\"pay\" />\n" +
"                                        <img src=\"https://ci6.googleusercontent.com/proxy/Xx89daIHJz5Ui3NyJT7hyYNRZQRSnkEfh3xNnx6WGvbOgWFewkYpWYV55StM7HY1dbUtxtx6RGolGp25pda-CGdg3ukXVXQQEhpUxngcZin2_4JI4wCie9sVrK0cgdzsk39pFN8ISUG1WSfYc-HYiJEohggS6Jf4NA=s0-d-e1-ft#http://www.cathaypacific.com/content/dam/cx/applications/ibe/amadeus/email/payment/other-paypal.png\" alt=\"pay\" />\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"foot-m\">\n" +
"                                    <div class=\"foot-m-l\">\n" +
"                                        <div class=\"foot-m-l-title\">\n" +
"                                            Tổng phí cần thanh toán:\n" +
"                                        </div>\n" +
"                                        <div class=\"foot-m-l-price\">\n" +
"                                            "+NumberFormat.getNumberInstance(Locale.US).format(price)+" <span>VND</span>\n" +
"                                        </div>\n" +
"                                    </div>\n" +
"                                    <div class=\"foot-m-r\">\n" +
"                                        <a href=\"http://"+ip()+":8080/checkorder.html?code="+idBoss+"&mailphone="+boss.getPhone()+"\">Xem chi tiết</a>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                            <div class=\"title\">Tóm tắt hành trình</div>\n" +
"                            <div class=\"table-tt\">\n" +
"                                <div class=\"head-table\">\n" +
"                                    <div class=\"row\">\n" +
"                                        <div class=\"table-kh\">Khởi hành</div>\n" +
"                                        <div class=\"table-tgkh\">Thời gian bay: <b>" + ttchuyendi.getTimeBay() +"</b></div>\n" +
"                                    </div>\n" +
"                                    <div class=\"row\">\n" +
"                                        <div class=\"table-dd1\">"+ttchuyendi.getDdName()+"</div>\n" +
"                                        <div class=\"table-ddf text-center\"><img src=\"https://ci6.googleusercontent.com/proxy/7AgrpZDDp1wLM5FZAYzU3FZ8iGBJECeGaol93hDs-oRDkFEflrr29P2Y4rlUsfxf3kwYNucXbfjaiwJ6Zcp_qdx8tgO8tjmVMpSlXaJY2-j2W3zSfY0uQvb5rRvI38vrVrJ1XdH2-qGkpuMr3x_C=s0-d-e1-ft#http://www.cathaypacific.com/content/dam/cx/applications/ibe/amadeus/email/icon-plane.png\" alt=\"bay\" /></div>\n" +
"                                        <div class=\"table-dd2\">"+ttchuyendi.getDdName2()+"</div>\n" +
"                                    </div>\n" +
"                                </div>\n" +
"                                <div class=\"table-tgdd\">\n" +
"                                    <div class=\"row\">\n" +
"                                        <div class=\"fulldate-table\">\n" +
"                                            "+ ttchuyendi.getDateFull() +"\n" +
"                                        </div>\n" +
"                                        <div class=\"dd-tg\">\n" +
"                                            "+ ttchuyendi.getDd()+" <b>"+ttchuyendi.getTimeDi()+"</b> "+ ttchuyendi.getDd2()+" <b>"+ttchuyendi.getTimeDd()+"</b>\n" +
"                                        </div>\n" +
"                                    </div>\n" +
"                                </div>\n" +
                                    ghe +
"                            </div>\n" +
"                            <div class=\"footer text-center\">\n" +
"                                <div class=\"qr-f\">\n" +
"                                    <div class=\"\">Quét để truy cập nhanh</div>\n" +
"                                    <img src=\"http://tools.sinhvienit.net/qrcode/image.php?data=http%3A%2F%2F"+ip()+"%3A8080%2Fcheckorder.html%3Fcode%3D"+idBoss+"%26mailphone%3D"+boss.getPhone()+ "&size=4&level=M\" alt=\"QR\" />\n" +
"                                </div>\n" +
"                                <div class=\"f-logo\">\n" +
"                                    <img src=\"https://ci6.googleusercontent.com/proxy/-b1wWvsj3EbK7PGbmnC8QTxd94enNDAiU1Kj-4lD8A-8XuV-MxG8CmGYflrZSs5s6ldi4Q=s0-d-e1-ft#http://i.imgur.com/7j3fTOJ.png\" alt=\"AptechAir\" />\n" +
"                                    <div class=\"div\">Một sản phẩm của KYNUTeam</div>\n" +
"                                </div>\n" +
"                            </div>\n" +
"                        </div>\n" +
"                        <div class=\"theend\"></div>\n" +
"                    </div>\n" +
"                </td>\n" +
"            </tr>\n" +
"        </tbody>\n" +
"    </table>\n" +
"</body>\n" +
"\n" +
"</html>";
        
        String SENDERS_EMAIL = "aptechair@gmail.com";
        String SENDERS_PWD = "fptaptech";
        String RECIPIENTS_EMAIL = boss.getEmail();
        Properties mailProps = new Properties();

        mailProps.put("mail.smtp.host", "smtp.gmail.com");
        mailProps.put("mail.smtp.port", "587");
        mailProps.put("mail.smtp.auth", "true");
        mailProps.put("mail.smtp.starttls.enable", "true");

        Authenticator authenticator = new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(SENDERS_EMAIL, SENDERS_PWD);
            }
        };

        Session session = Session.getInstance(mailProps, authenticator);
        //When I used method Session.getInstance instead Session.getDefaultInstance in every test,
        //errors disappeared.

        try {
            final MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(SENDERS_EMAIL,"AptechAir"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(RECIPIENTS_EMAIL));
            message.setSubject("[AptechAir] Xác nhận Đặt chỗ thành công - Mã " + idBoss);
            message.setContent(contentv2, "text/html; charset=UTF-8");
            System.out.println("Đang gửi mail vui lòng chờ...");
            Transport.send(message);

            System.out.println("Check inbox kìa!");

        } catch (Exception e) {
            System.err.println("Problem sending email. Exception : " + e.getMessage());
            return false;
        }
        return true;
    }
    
    public static String ip(){
         String ip="";
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                // filters out 127.0.0.1 and inactive interfaces
                if (iface.isLoopback() || !iface.isUp()) {
                    continue;
                }

                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();

                    // *EDIT*
                    if (addr instanceof Inet6Address) {
                        continue;
                    }

                    ip = addr.getHostAddress();
                    System.out.println(ip);
                }
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
        return ip;
    }

    public static String deadline(int id) {
        String dateinput = BossModel.getTime(id);
        try {

            SimpleDateFormat datesql = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat formatterSH = new SimpleDateFormat("HH:mm");

            Date date = datesql.parse(dateinput);
            String datenomal = formatter.format(date).toString();
            //Chuyển datesql về dạng + - được.
            String hours = formatterSH.format(date).toString();

            DateTimeFormatter formatter3 = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            LocalDate datelo = LocalDate.parse(datenomal, formatter3);
            String dateI = datelo.plusDays(1).format(formatter3);
            datelo = LocalDate.parse(dateI, formatter3);

            //String dateI = datelo.plusDays(1).format(datesql);
            Locale vn = new Locale("vi", "VN");
            DayOfWeek w = datelo.getDayOfWeek();
            Month thang = datelo.getMonth();
            int ngaythang = datelo.getDayOfMonth();
            String ngay = w.getDisplayName(TextStyle.FULL, vn);
            String thangV = thang.getDisplayName(TextStyle.NARROW, vn);

            dateinput = hours + " " + ngay + ", ngày " + ngaythang + " tháng " + thangV + " năm " + datelo.getYear();

        } catch (ParseException ex) {
            Logger.getLogger(SentMailv2Model.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dateinput;
    }

    public static void main(String[] args) {
        //  sentmail("letiep97@gmail.com");
        System.out.println("Dealine: " + deadline(52));
    }

}
