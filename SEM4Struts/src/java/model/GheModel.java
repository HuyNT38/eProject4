/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Seat;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author letiep97
 */
public class GheModel {
    
    public static ArrayList<Seat> listSeat (){
        ArrayList<Seat> list = new ArrayList();
        String sql = "SELECT * FROM tblGhe ORDER BY _order ASC";

        try {
            Statement st = ConnectionFactory.getConnection().createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                list.add(new Seat(rs.getString(1), rs.getInt(2), rs.getInt(3), 0));
            }
            rs.close();
        } catch (SQLException ex) {
        }

        return list;
    }

    public static Seat getSeat(String id) {
        Seat temp = new Seat();
        String sql = "SELECT * FROM tblGhe WHERE _id=?";
        try {
            Connection conn = ConnectionFactory.getConnection();
            PreparedStatement prst = conn.prepareStatement(sql);
            prst.setString(1, id);
            ResultSet rs = prst.executeQuery();
            while (rs.next()) {

                int type = rs.getInt(2);
                int price = rs.getInt(3);

                temp = new Seat(id, type, price, 0);
            }
            conn.close();
            prst.close();
            rs.close();
        } catch (SQLException ex) {
        }
        return temp;
    }

    public static int getType(String id) {
        Seat seat = getSeat(id);
        return seat.getType();
    }

    public static int getPrice(String id) {
        Seat seat = getSeat(id);
        return seat.getPrice();
    }
    
    public static void main(String[] args) {
        System.out.println("Price: "+getPrice("3D"));
        System.out.println("Type: "+getType("3D"));
        for (int i = 1; i < listSeat().size()+1; i++) {
            if (i%6!=0) {
                System.out.print(listSeat().get(i-1).getId()+" - ");
            } else
                System.out.println(listSeat().get(i-1).getId());
            
        }
    }
}
