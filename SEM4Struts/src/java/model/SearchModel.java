/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.EmBe;
import entity.KhachHang;
import entity.KhachHangCustom;
import entity.ThongTinChuyenDi;
import java.util.ArrayList;

/**
 *
 * @author letiep97
 */
public class SearchModel {

    public static ArrayList<KhachHangCustom> listKhachHang(int id) {
        ArrayList<KhachHang> listKh = new ArrayList<>();
        ArrayList<KhachHangCustom> list = new ArrayList<>();

        listKh = KhachHangModel.getListbyidB(id);

        for (int i = 0; i < listKh.size(); i++) {
            KhachHangCustom temp = new KhachHangCustom();
            temp.setId(listKh.get(i).getId());
            temp.setFname(listKh.get(i).getFname());
            temp.setLname(listKh.get(i).getLname());
            temp.setType(listKh.get(i).getType());
            temp.setGender(listKh.get(i).getGender());
            temp.setIdG(VeModel.getidGbyidKh(listKh.get(i).getId()));
            temp.setIdMB(VeModel.getidCBbyidB(id));
            temp.setPriceG(GheModel.getPrice(VeModel.getidGbyidKh(listKh.get(i).getId())));
            temp.setTypeG(GheModel.getType(VeModel.getidGbyidKh(listKh.get(i).getId())));

            list.add(temp);

            if (listKh.get(i).getType() == 1) {
                EmBe ee = EmBeModel.byidKH(listKh.get(i).getId());
                if (ee != null) {
                    temp = new KhachHangCustom();
                    temp.setFname(ee.getFname());
                    temp.setLname(ee.getLname());
                    temp.setWith(ee.getIdKH());
                    temp.setDate(ee.getBirthday());
                    temp.setType(3);
                    list.add(temp);
                }
            }

        }
        return list;
    }
    
    public static ThongTinChuyenDi ttChuyenDi(int id){
        ThongTinChuyenDi emp = new ThongTinChuyenDi();
        
        int idCB = VeModel.getidCBbyidB(id);
        emp.setDd(ChuyenBayModel.getDDi(idCB));
        emp.setDd2(ChuyenBayModel.getDDd(idCB));
        emp.setName(ChuyenBayModel.getName(idCB));
        emp.setDdName(ChuyenBayModel.getNameDDi(idCB));
        emp.setDdName2(ChuyenBayModel.getNameDDd(idCB));
        emp.setPrice(ChuyenBayModel.getPrice(idCB));
        emp.setPriceeb(EmBe.priceeb);
        emp.setTimeBay(ChuyenBayModel.getTimeBaybyID(idCB));
        emp.setTimeDi(ChuyenBayModel.getTimeDi(idCB));
        emp.setTimeDd(ChuyenBayModel.getTimeDd(idCB));
        emp.setDateFull(ChuyenBayModel.getDateFull(idCB));
        emp.setNl(KhachHangModel.sizeNLbyidB(id));
        emp.setTe(KhachHangModel.sizeTEbyidB(id));
        emp.setEb(EmBeModel.sizebyidB(id));
        
        return emp;
    }

    public static void main(String[] args) {
        ArrayList<KhachHangCustom> khachhang = listKhachHang(64);
        for (int i = 0; i < khachhang.size(); i++) {
            if (khachhang.get(i).getType() == 1 || khachhang.get(i).getType() == 2) {
                System.out.println("" + khachhang.get(i).getFname() + khachhang.get(i).getLname());
                System.out.print(" idG: " + khachhang.get(i).getIdG());
                System.out.println(" idMB: " + khachhang.get(i).getIdMB());
                if (khachhang.get(i).getType() == 1) {
                    for (int j = 0; j < khachhang.size(); j++) {
                        if (khachhang.get(i).getId() == khachhang.get(j).getWith() && khachhang.get(j).getType() == 3) {
                            System.out.println("---" + khachhang.get(j).getFname() + khachhang.get(j).getLname());
                        }
                    }
                }
            }
        }
        System.out.println("date: "+ttChuyenDi(64).getDateFull() +" EB:"+ ttChuyenDi(64).getEb());
    }
}
