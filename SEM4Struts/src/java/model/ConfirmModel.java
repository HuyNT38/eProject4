/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.mysql.jdbc.Connection;
import entity.Boss;
import entity.EmBe;
import entity.KhachHang;
import entity.KhachHangCustom;
import entity.ThongTinChuyenDi;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author letiep97
 */
public class ConfirmModel {

    public static int addBoss(Boss boss, int price, String date) {
        String sql2 = "INSERT INTO `tblBoss`(`_id`, `_fname`, `_lname`, `_email`, `_phone`, `_datetime`, `_price`, `_status`) "
                + "VALUES (null,'" + boss.getFname() + "','" + boss.getLname() + "','" + boss.getEmail() + "',"
                + "'" + boss.getPhone() + "','" + date + "',"+price+",0)";
        int id = 0;
        try {
            Connection conn = ConnectionFactory.getConnection();
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(sql2, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs2 = stmt.getGeneratedKeys();
            if (rs2.next()) {
                id = rs2.getInt(1);
            }
            rs2.close();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return id;
    }

    public static int addKhachHang(KhachHang kh, int idB) {
        String sql2 = "INSERT INTO tblHanhKhach(`_id`, `_idB`, `_fname`, `_lname`, `_type`, `_gender`) "
                + "VALUES (null," + idB + ",'" + kh.getFname() + "','" + kh.getLname() + "'," + kh.getType() + "," + kh.getGender() + ")";
        int id = 0;
        System.out.println("sqlKH: "+sql2);
        try {
            Connection conn = ConnectionFactory.getConnection();
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(sql2, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs2 = stmt.getGeneratedKeys();
            if (rs2.next()) {
                id = rs2.getInt(1);
            }
            rs2.close();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return id;
    }



    public static int addVe(int idCB, String idG, int idKH, int idB) {
        String sql = "INSERT INTO `tblVe` "
                + "VALUES (null," + idCB + ",'" + idG + "'," + idKH + "," + idB + ")";
        int id = 0;
        System.out.println("sqlVe: "+sql);
        try {
            Connection conn = ConnectionFactory.getConnection();
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs2 = stmt.getGeneratedKeys();
            if (rs2.next()) {
                id = rs2.getInt(1);
            }
            rs2.close();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
        }
        return id;
    }

    public static String Comfirm(ThongTinChuyenDi ttchuyendi, Boss boss, ArrayList<KhachHangCustom> khachhang) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        //System.out.println(dateFormat.format(date)); //2017-08-17 24:00:00
        String datetime = dateFormat.format(date);
        
        int price = Total(ttchuyendi, khachhang);

        
        int idB = addBoss(boss,price,datetime);

        for (int i = 0; i < khachhang.size(); i++) {
            if (khachhang.get(i).getType() == 1 || khachhang.get(i).getType() == 2) {
                int idKH = addKhachHang(new KhachHang(0, idB, khachhang.get(i).getFname(),
                        khachhang.get(i).getLname(), khachhang.get(i).getType(),
                        khachhang.get(i).getGender()), idB);
                if (khachhang.get(i).getType() == 1) {
                    for (int j = 0; j < khachhang.size(); j++) {
                        if (khachhang.get(i).getId() == khachhang.get(j).getWith() && khachhang.get(j).getType() == 3) {
                            EmBeModel.addEmbe(new EmBe(0, idKH, khachhang.get(j).getFname(), khachhang.get(j).getLname(),
                                    khachhang.get(j).getDate(), khachhang.get(j).getGender(), idB));
                        }
                    }
                }
                addVe(ttchuyendi.getId(), khachhang.get(i).getIdG(), idKH, idB);
            }
        }

        return String.format("%09d", idB);
    }
    
    public static int Total(ThongTinChuyenDi ttchuyendi, ArrayList<KhachHangCustom> khachhang){
        int total=0;
        for (int i = 0; i < khachhang.size(); i++) {
            if (khachhang.get(i).getType()==1||khachhang.get(i).getType()==2) {
                total+=GheModel.getPrice(khachhang.get(i).getIdG());
            }
        }
        total += (ttchuyendi.getNl()+ttchuyendi.getTe())*ChuyenBayModel.getPrice(ttchuyendi.getId())
                +ttchuyendi.getEb()*ttchuyendi.getPriceeb();
        return total;
    }

    public static void main(String[] args) {
        int idB = addBoss(new Boss(0, "Tiep", "Le", "letiep97@gmail.com", "113", "",1,1),1300000,"2017-08-17 11:59:05.0");
        System.out.println("id idB: " + idB);
        int idKH = addKhachHang(new KhachHang(0, idB, "Khach", "hang", 1, 1), idB);
        // System.out.println("" + addKhachHang(new KhachHang(0, idB, "Khach", "hang", 1, 1), idB));

      //  System.out.println("add Embe: " + EmBeModel.addEmbe(new EmBe(1, 100, "Em", "Be2", "2016-3-13")));
        System.out.println("add Ve: " + String.format("%09d", addVe(10, "5D", idKH, 60)));

    }
}
