/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.mysql.jdbc.Connection;
import entity.Ve;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author letiep97
 */
public class VeModel {

    public static Ve getVebyidB(int id) {
        Ve temp = new Ve();
        String sql = "SELECT * FROM tblVe WHERE _idB=?";
        try {
            Connection conn = ConnectionFactory.getConnection();
            PreparedStatement prst = conn.prepareStatement(sql);
            prst.setInt(1, id);
            ResultSet rs = prst.executeQuery();
            while (rs.next()) {
                temp = new Ve(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getInt(5));
            }
            conn.close();
            prst.close();
            rs.close();
        } catch (SQLException ex) {
        }
        return temp;
    }

    public static Ve getVebyidKH(int id) {
        Ve temp = new Ve();
        String sql = "SELECT * FROM tblVe WHERE _idKH=?";
        try {
            Connection conn = ConnectionFactory.getConnection();
            PreparedStatement prst = conn.prepareStatement(sql);
            prst.setInt(1, id);
            ResultSet rs = prst.executeQuery();
            while (rs.next()) {
                temp = new Ve(rs.getInt(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getInt(5));
            }
            conn.close();
            prst.close();
            rs.close();
        } catch (SQLException ex) {
        }
        return temp;
    }

    public static int getidCBbyidB(int id) {
        return getVebyidB(id).getIdCB();
    }

    public static String getidGbyidKh(int id) {
        return getVebyidKH(id).getIdG();
    }

    public static void main(String[] args) {
        System.out.println("getVebyidB: " + getVebyidB(64).getIdCB());
        System.out.println("getidGbyidKh: " + getidGbyidKh(161));
    }
}
