/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.DiaDiem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author letiep97
 */
public class DiaDiemModel {

    public DiaDiem getByID(String id) throws SQLException {
        DiaDiem temp = new DiaDiem();
        String sql = "SELECT * FROM Item WHERE ID=? ORDER BY date DESC";
        PreparedStatement prst = ConnectionFactory.getConnection().prepareStatement(sql);
        prst.setString(1, id);
        ResultSet rs = prst.executeQuery();
        while (rs.next()) {
            String date = rs.getString(7);
            temp = new DiaDiem(id, date);

        }
        rs.close();
        return temp;
    }
    
        public static String getNameDDbyId(String id) {
        String sql = "SELECT _name FROM tblDiaDiem WHERE _id=?";
        String name = "";
        try {
            Connection conn = ConnectionFactory.getConnection();
            PreparedStatement prst = conn.prepareStatement(sql);
            prst.setString(1, id);
            ResultSet rs = prst.executeQuery();

//            Statement st = ConnectionFactory.getConnection().createStatement();
//            ResultSet rs = st.executeQuery("SELECT _name FROM tblDiaDiem WHERE _id="+id);
            while (rs.next()) {
                name = rs.getString(1);
            }
            prst.close();
            rs.close();
            conn.close();
        } catch (SQLException ex) {
        }
        return name;
    }
    
    public static String getNameDDbyidMB(int id){
        String sql = "SELECT _name FROM tblDiaDiem WHERE _id=?";
        String name = "";
        
        /////
        return name;
    }
}
