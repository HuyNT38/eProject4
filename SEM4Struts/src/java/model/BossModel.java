/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.Boss;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author letiep97
 */
public class BossModel {
    
    public static ArrayList<Boss> listBoss(){
        ArrayList<Boss> list = new ArrayList<>();
        try {
            String sql = "SELECT * FROM tblBoss ORDER BY _datetime DESC";
            Connection conn = ConnectionFactory.getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                Boss b = new Boss(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5),
                        rs.getString(6), rs.getInt(7), rs.getInt(8));
                list.add(b);
            }

            st.close();
            rs.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(BossModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
    public static boolean confirmBoss(String type, String id) {
        int result = 0;
        String sql = "UPDATE tblBoss SET _status=? WHERE _id=?";
        try {
            PreparedStatement prst = ConnectionFactory.getConnection().prepareStatement(sql);
            prst.setInt(1, Integer.valueOf(type));
            prst.setInt(2, Integer.valueOf(id));
            result = prst.executeUpdate();
            prst.close();
        } catch (SQLException e) {

        }
        return result > 0;
    }
    


    public static Boss getBoss(int id) {
        Boss emp = null;
        try {
            String sql = "SELECT * FROM tblBoss WHERE _id=" + id;
            Connection conn = ConnectionFactory.getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                emp = new Boss(id, rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8));
            }

            st.close();
            rs.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(BossModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return emp;
    }

    public static Boss searchBoss(int id, String email) {
        Boss emp = null;
        try {
            String sql = "SELECT * FROM tblBoss WHERE (_email='" + email+"' OR _phone='" + email+"') AND _id=" + id;
            Connection conn = ConnectionFactory.getConnection();
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                emp = new Boss(rs.getInt(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8));
            }

            st.close();
            rs.close();
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(BossModel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return emp;
    }


    public static String getTime(int id) {
        Boss boss = new Boss();
        BossModel bm = new BossModel();
        boss = bm.getBoss(id);
        return boss.getTime();
    }

    public static void main(String[] args) {
        int i = 86;
//        System.out.println("Fname: " + getBoss(i).getFname() + " lname: " + getBoss(i).getLname() + " time: "
//                + getBoss(i).getTime() + " email: " + getBoss(i).getEmail() + " phone: " + getBoss(i).getPhone() + " price: "
//                + getBoss(i).getPrice());
        System.out.println("GetTime: " + getTime(i));
        System.out.println("i: " + searchBoss(i,"01233668804").getId());
        System.out.println("SearchBoss: " + searchBoss(i,"01233668804").getStatus());
        ArrayList<Boss> list = new ArrayList<Boss>();
        list = listBoss();
        System.out.println("size listBoss: " + list.size());
        
        for (Boss list1 : list) {
            System.out.println("" + list1.getId());
        }
        confirmBoss("1","105");
    }
}
