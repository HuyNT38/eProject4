/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import entity.KhachHang;
import entity.Seat;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author letiep97
 */
public class KhachHangModel {

    public static ArrayList<KhachHang> getListbyidB(int id) {

        ArrayList<KhachHang> list = new ArrayList<>();

        String sql = "SELECT * FROM tblHanhKhach WHERE _idB=?";
        try {
            Connection conn = ConnectionFactory.getConnection();
            PreparedStatement prst = conn.prepareStatement(sql);
            prst.setInt(1, id);
            ResultSet rs = prst.executeQuery();
            while (rs.next()) {
                KhachHang temp
                        = new KhachHang(rs.getInt(1), rs.getInt(2), rs.getString(3),
                                rs.getString(4), rs.getInt(5), rs.getInt(6));

                list.add(temp);
            }
            conn.close();
            prst.close();
            rs.close();
        } catch (SQLException ex) {
        }
        return list;
    }

    public static int sizeRowbyidB(int id, int type) {

        int count = 0;

        String sql = "SELECT COUNT(*) FROM tblHanhKhach WHERE  _idB=? AND _type=?";
        try {
            Connection conn = ConnectionFactory.getConnection();
            PreparedStatement prst = conn.prepareStatement(sql);
            prst.setInt(1, id);
            prst.setInt(2, type);
            ResultSet rs = prst.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            conn.close();
            prst.close();
            rs.close();
        } catch (SQLException ex) {
        }
        return count;
    }

    public static int sizeNLbyidB(int id) {
        return sizeRowbyidB(id, 1);
    }

    public static int sizeTEbyidB(int id) {
        return sizeRowbyidB(id, 2);
    }

    public static void main(String[] args) {
        ArrayList<KhachHang> list = getListbyidB(64);
        for (int i = 0; i < list.size(); i++) {
            System.out.println("Khach hang: " + list.get(i).getFname());
        }
        
        System.out.println("NL: " +sizeNLbyidB(64));
        System.out.println("Te: " +sizeTEbyidB(64));
    }
}
