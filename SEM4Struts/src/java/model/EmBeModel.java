/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.mysql.jdbc.Connection;
import entity.EmBe;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author letiep97
 */
public class EmBeModel {
    
        public static int addEmbe(EmBe eb) {
        String sql = "INSERT INTO `tblEmBe`"
                + "VALUES (null," + eb.getIdKH() + ",'" + eb.getFname() + "','" + eb.getLname() + "','" + eb.getBirthday() 
                + "',"+eb.getGender()+", "+eb.getIdB()+")";
        int id = 0;
        try {
            Connection conn = ConnectionFactory.getConnection();
            Statement stmt = conn.createStatement();
            stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs2 = stmt.getGeneratedKeys();
            if (rs2.next()) {
                id = rs2.getInt(1);
            }
            rs2.close();
            stmt.close();
            conn.close();
        } catch (SQLException ex) {
        }
        return id;
    }

    public static EmBe byidKH(int id) {
        EmBe temp = null ;
        String sql = "SELECT * FROM tblEmBe WHERE _idKH=?";
        try {
            Connection conn = ConnectionFactory.getConnection();
            PreparedStatement prst = conn.prepareStatement(sql);
            prst.setInt(1, id);
            ResultSet rs = prst.executeQuery();
            while (rs.next()) {
                temp = new EmBe(rs.getInt(1), rs.getInt(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getInt(6), rs.getInt(7));
            }
            conn.close();
            prst.close();
            rs.close();
        } catch (SQLException ex) {
        }
        return temp;
    }
    
        public static int sizebyidB(int id) {
        int count = 0;

        String sql = "SELECT COUNT(*) FROM tblEmBe WHERE  _idB=?";
        try {
            java.sql.Connection conn = ConnectionFactory.getConnection();
            PreparedStatement prst = conn.prepareStatement(sql);
            prst.setInt(1, id);
            ResultSet rs = prst.executeQuery();
            while (rs.next()) {
                count = rs.getInt(1);
            }
            conn.close();
            prst.close();
            rs.close();
        } catch (SQLException ex) {
        }
        return count;
    }

    public static void main(String[] args) {
        System.out.println("byidKH: " + byidKH(177).getBirthday());
        System.out.println("sizeEb: " + sizebyidB(64));
    }
}
