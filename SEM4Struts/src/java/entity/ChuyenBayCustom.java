/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author letiep97
 */
public class ChuyenBayCustom {
    private int id;
    private String nameDDi;
    private String nameDDd;
    private String DDi;
    private String DDd;
    private String name;
    private String dateDi;
    private String dateDen;
    private String timeDi;
    private String timeDen;
    private String timeBay;
    private int price;
    private int type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameDDi() {
        return nameDDi;
    }

    public void setNameDDi(String nameDDi) {
        this.nameDDi = nameDDi;
    }

    public String getNameDDd() {
        return nameDDd;
    }

    public void setNameDDd(String nameDDd) {
        this.nameDDd = nameDDd;
    }

    public String getDDi() {
        return DDi;
    }

    public void setDDi(String DDi) {
        this.DDi = DDi;
    }

    public String getDDd() {
        return DDd;
    }

    public void setDDd(String DDd) {
        this.DDd = DDd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDateDi() {
        return dateDi;
    }

    public void setDateDi(String dateDi) {
        this.dateDi = dateDi;
    }

    public String getDateDen() {
        return dateDen;
    }

    public void setDateDen(String dateDen) {
        this.dateDen = dateDen;
    }

    public String getTimeDi() {
        return timeDi;
    }

    public void setTimeDi(String timeDi) {
        this.timeDi = timeDi;
    }

    public String getTimeDen() {
        return timeDen;
    }

    public void setTimeDen(String timeDen) {
        this.timeDen = timeDen;
    }

    public String getTimeBay() {
        return timeBay;
    }

    public void setTimeBay(String timeBay) {
        this.timeBay = timeBay;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public ChuyenBayCustom(int id, String nameDDi, String nameDDd, String DDi, String DDd, String name, String dateDi, String dateDen, String timeDi, String timeDen, String timeBay, int price, int type) {
        this.id = id;
        this.nameDDi = nameDDi;
        this.nameDDd = nameDDd;
        this.DDi = DDi;
        this.DDd = DDd;
        this.name = name;
        this.dateDi = dateDi;
        this.dateDen = dateDen;
        this.timeDi = timeDi;
        this.timeDen = timeDen;
        this.timeBay = timeBay;
        this.price = price;
        this.type = type;
    }
    
    

    public ChuyenBayCustom() {
    }

}
