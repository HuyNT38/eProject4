/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author letiep97
 */
public class EmBe {
    public static final int priceeb = 150000;
    private int id;
    private int idKH;
    private String fname;
    private String lname;
    private String birthday;
    private int gender;
    private int idB;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdKH() {
        return idKH;
    }

    public void setIdKH(int idKH) {
        this.idKH = idKH;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getIdB() {
        return idB;
    }

    public void setIdB(int idB) {
        this.idB = idB;
    }

    public EmBe(int id, int idKH, String fname, String lname, String birthday, int gender, int idB) {
        this.id = id;
        this.idKH = idKH;
        this.fname = fname;
        this.lname = lname;
        this.birthday = birthday;
        this.gender = gender;
        this.idB = idB;
    }

    public EmBe() {
    }
   
    

    
}
