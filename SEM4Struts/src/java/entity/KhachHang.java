/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author letiep97
 */
public class KhachHang {
    private int id;
    private int idB;
    private String fname;
    private String lname;
    private int type;
    private int gender;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdB() {
        return idB;
    }

    public void setIdB(int idB) {
        this.idB = idB;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public KhachHang(int id, int idB, String fname, String lname, int type, int gender) {
        this.id = id;
        this.idB = idB;
        this.fname = fname;
        this.lname = lname;
        this.type = type;
        this.gender = gender;
    }

    public KhachHang() {
    }
    
    
    
}
