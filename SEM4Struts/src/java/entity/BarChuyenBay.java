/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author letiep97
 */
public class BarChuyenBay {
    private String day;
    private String dayS;
    private String date;
    private String dateS;
    private String dateFull;
    private int price;
    private String url;

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDayS() {
        return dayS;
    }

    public void setDayS(String dayS) {
        this.dayS = dayS;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDateS() {
        return dateS;
    }

    public void setDateS(String dateS) {
        this.dateS = dateS;
    }

    public String getDateFull() {
        return dateFull;
    }

    public void setDateFull(String dateFull) {
        this.dateFull = dateFull;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public BarChuyenBay(String day, String dayS, String date, String dateS, String dateFull, int price, String url) {
        this.day = day;
        this.dayS = dayS;
        this.date = date;
        this.dateS = dateS;
        this.dateFull = dateFull;
        this.price = price;
        this.url = url;
    }

    public BarChuyenBay() {
    }
    
    
    
}
