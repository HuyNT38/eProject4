/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;

/**
 *
 * @author letiep97
 */
public class KhachHangCustom implements Serializable {

    private int id;
    private int idB;
    private int idMB;
    private String fname;
    private String lname;
    private String date;
    private int type;
    private int gender;
    private int with;
    private String idG;
    private int priceG;
    private int typeG;

    public KhachHangCustom() {
    }

    public KhachHangCustom(int id, int idB, int idMB, String fname, String lname, String date, int type, int gender, int with, String idG, int priceG, int typeG) {
        this.id = id;
        this.idB = idB;
        this.idMB = idMB;
        this.fname = fname;
        this.lname = lname;
        this.date = date;
        this.type = type;
        this.gender = gender;
        this.with = with;
        this.idG = idG;
        this.priceG = priceG;
        this.typeG = typeG;
    }
    
    

    public int getTypeG() {
        return typeG;
    }

    public void setTypeG(int typeG) {
        this.typeG = typeG;
    }

    public int getPriceG() {
        return priceG;
    }

    public void setPriceG(int priceG) {
        this.priceG = priceG;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdB() {
        return idB;
    }

    public void setIdB(int idB) {
        this.idB = idB;
    }

    public int getIdMB() {
        return idMB;
    }

    public void setIdMB(int idMB) {
        this.idMB = idMB;
    }

    public String getIdG() {
        return idG;
    }

    public void setIdG(String idG) {
        this.idG = idG;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getWith() {
        return with;
    }

    public void setWith(int with) {
        this.with = with;
    }

}
