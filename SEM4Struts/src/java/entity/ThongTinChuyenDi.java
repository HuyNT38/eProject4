/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;

/**
 *
 * @author letiep97
 */
public class ThongTinChuyenDi implements Serializable {

    private String name;
    private String ddName;
    private String dd;
    private String ddName2;
    private String dd2;
    private String dateFull;
    private String timeDi;
    private String timeDd;
    private String timeBay;
    private int id;
    private int nl;
    private int te;
    private int eb;
    private int price;
    private int priceeb;

    public String getTimeDi() {
        return timeDi;
    }

    public void setTimeDi(String timeDi) {
        this.timeDi = timeDi;
    }

    public String getTimeDd() {
        return timeDd;
    }

    public void setTimeDd(String timeDd) {
        this.timeDd = timeDd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDd() {
        return dd;
    }

    public void setDd(String dd) {
        this.dd = dd;
    }

    public String getDd2() {
        return dd2;
    }

    public void setDd2(String dd2) {
        this.dd2 = dd2;
    }

    public String getTimeBay() {
        return timeBay;
    }

    public void setTimeBay(String timeBay) {
        this.timeBay = timeBay;
    }

    public int getPriceeb() {
        return priceeb;
    }

    public void setPriceeb(int priceeb) {
        this.priceeb = priceeb;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDdName() {
        return ddName;
    }

    public void setDdName(String ddName) {
        this.ddName = ddName;
    }

    public String getDdName2() {
        return ddName2;
    }

    public void setDdName2(String ddName2) {
        this.ddName2 = ddName2;
    }

    public String getDateFull() {
        return dateFull;
    }

    public void setDateFull(String dateFull) {
        this.dateFull = dateFull;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNl() {
        return nl;
    }

    public void setNl(int nl) {
        this.nl = nl;
    }

    public int getTe() {
        return te;
    }

    public void setTe(int te) {
        this.te = te;
    }

    public int getEb() {
        return eb;
    }

    public void setEb(int eb) {
        this.eb = eb;
    }

    public ThongTinChuyenDi() {
    }

    public ThongTinChuyenDi(String ddName, String ddName2, String dateFull, int id, int nl, int te, int eb) {
        this.ddName = ddName;
        this.ddName2 = ddName2;
        this.dateFull = dateFull;
        this.id = id;
        this.nl = nl;
        this.te = te;
        this.eb = eb;
    }

}
