/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author letiep97
 */
public class Ve {

    private int id;
    private int idCB;
    private String idG;
    private int idKH;
    private int idB;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCB() {
        return idCB;
    }

    public void setIdCB(int idCB) {
        this.idCB = idCB;
    }

    public String getIdG() {
        return idG;
    }

    public void setIdG(String idG) {
        this.idG = idG;
    }

    public int getIdKH() {
        return idKH;
    }

    public void setIdKH(int idKH) {
        this.idKH = idKH;
    }

    public int getIdB() {
        return idB;
    }

    public void setIdB(int idB) {
        this.idB = idB;
    }

    public Ve() {
    }

    public Ve(int id, int idCB, String idG, int idKH, int idB) {
        this.id = id;
        this.idCB = idCB;
        this.idG = idG;
        this.idKH = idKH;
        this.idB = idB;
    }

}
